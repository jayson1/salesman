module salesman {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires lombok;
    requires java.persistence;
    requires org.hibernate.orm.core;
    requires spring.data.commons;
    requires spring.data.jpa;
    requires shiro.core;
    requires shiro.spring;
    requires spring.beans;
    requires spring.context;
    requires com.jfoenix;
    requires org.slf4j;
    requires java.sql;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.aop;

//  opens com.james.salesman to javafx.fxml, javafx.base;
    opens com.james.salesman;
    exports com.james.salesman;

}