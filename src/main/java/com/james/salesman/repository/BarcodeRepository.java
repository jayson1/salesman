package com.james.salesman.repository;

import com.james.salesman.model.Barcode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BarcodeRepository extends JpaRepository< Barcode, Long > { }
