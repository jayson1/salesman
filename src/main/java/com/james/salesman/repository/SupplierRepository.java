package com.james.salesman.repository;

import com.james.salesman.model.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplierRepository extends JpaRepository< Supplier, Long > { }
