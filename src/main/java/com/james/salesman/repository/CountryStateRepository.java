package com.james.salesman.repository;

import com.james.salesman.model.CountryState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryStateRepository extends JpaRepository< CountryState, Long > { }
