package com.james.salesman.repository;

import com.james.salesman.model.SaleItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesItemRepository extends JpaRepository< SaleItem, Long> { }
