package com.james.salesman.repository;

import com.james.salesman.model.MeasurementUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeasurementUnitRepository extends JpaRepository< MeasurementUnit, Long > { }
