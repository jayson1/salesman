package com.james.salesman.repository;

import com.james.salesman.model.UnitConversion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnitConversionRepository extends JpaRepository< UnitConversion, Long > { }
