package com.james.salesman.repository;

import com.james.salesman.model.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductCategoryRepository extends JpaRepository< ProductCategory, Long > { }
