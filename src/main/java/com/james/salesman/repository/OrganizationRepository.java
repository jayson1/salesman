package com.james.salesman.repository;

import com.james.salesman.model.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizationRepository extends JpaRepository< Organization, Long > { }
