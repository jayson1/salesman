package com.james.salesman.repository;

import com.james.salesman.model.Expenses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpensesRepository extends JpaRepository< Expenses, Long > { }
