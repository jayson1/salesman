package com.james.salesman.repository;

import com.james.salesman.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository  extends JpaRepository< Permission, Long > { }
