package com.james.salesman.repository;

import com.james.salesman.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends JpaRepository< Currency, Long > { }
