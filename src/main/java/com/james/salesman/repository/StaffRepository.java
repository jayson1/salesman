package com.james.salesman.repository;

import com.james.salesman.model.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffRepository extends JpaRepository< Staff, Long> { }
