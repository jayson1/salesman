package com.james.salesman.configuration;

import com.james.salesman.utils.StageManager;
import java.io.IOException;
import javafx.stage.Stage;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.mgt.DefaultSessionManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration(proxyBeanMethods=true)
public class SpringConfig {
	@Autowired
	SpringFxmlLoader springFxmlLoader;

	@Bean
	@Lazy()
	public StageManager stageManager(Stage stage) throws IOException{
		return new StageManager(springFxmlLoader, stage);
	}

	@Bean @Lazy()
	public CustomCredentials credentialMatcher(){
		return new CustomCredentials();
	}

	@Bean
	@Lazy()
	public Realm customRealm() {
		CustomRealm customRealm = new CustomRealm();
		customRealm.setCredentialsMatcher(credentialMatcher());
		return customRealm;
	}

	@Bean
	@Lazy()
	protected SessionManager sessionManager() {
		DefaultSessionManager sessionManager = new DefaultSessionManager();
		sessionManager.setGlobalSessionTimeout(10800000);
		return sessionManager;
	}


	@Bean
	@Lazy()
	public DefaultSecurityManager securityManager(){
		DefaultSecurityManager securityManager = new DefaultSecurityManager();
		securityManager.setRealm(this.customRealm());
		securityManager.setSessionManager(sessionManager());
		SecurityUtils.setSecurityManager(securityManager);
		return securityManager;
	}


	/**
	 *Turn  on shiro aop annotation support such as @RequiresRoles,@RequiresPermissions
	 * Use proxy; therefore, code support needs to be turned on;
	 * @return authorizationAttributeSourceAdvisor
	 */
	@Bean
	@Lazy()
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(){
		AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
		authorizationAttributeSourceAdvisor.setSecurityManager(securityManager());
		return authorizationAttributeSourceAdvisor;
	}

	@Bean
	@Lazy()
	public MethodInvokingFactoryBean methodInvokingFactoryBean(){
		MethodInvokingFactoryBean methodInvokingFactoryBean = new MethodInvokingFactoryBean();
		methodInvokingFactoryBean.setStaticMethod("org.apache.shiro.SecurityUtils.setSecurityManager");
		methodInvokingFactoryBean.setArguments(new Object[]{securityManager()});
		return methodInvokingFactoryBean;
	}

	@Bean
	@Lazy()
	public CacheManager cacheManager() {
		return new MemoryConstrainedCacheManager();
	}
}
