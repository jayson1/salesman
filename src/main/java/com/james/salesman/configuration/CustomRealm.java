package com.james.salesman.configuration;

import com.james.salesman.constant.ErrorMessage;
import com.james.salesman.model.User;
import com.james.salesman.service.UserServiceImpl;
import java.util.Set;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

public class CustomRealm extends AuthorizingRealm {
	@Autowired
	private UserServiceImpl userService;

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
		SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
		String principal = (String) principalCollection.getPrimaryPrincipal();
		User user = userService.findByUsername(principal);
		if( user !=null ){
			Set< String > roles = this.userService.getUserRoleString(user);
			Set<String> permissions = this.userService.getUserPermissionsString(user);
			simpleAuthorizationInfo.setRoles(roles);
			simpleAuthorizationInfo.setStringPermissions(permissions);
		}
		return simpleAuthorizationInfo;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
		if (!(authenticationToken instanceof UsernamePasswordToken)) {
			throw new IllegalStateException(ErrorMessage.TOKEN_MISMATCH);
		}
		final UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
		if (token.getUsername() == null) {
			throw new AccountException(ErrorMessage.USERNAME_REQUIRED);
		}
		/*Authenticate user credentials*/
		final User user = this.userService.authenticateUser(token.getUsername(), String.valueOf(token.getPassword()));
		return new SimpleAuthenticationInfo(user, user.getPassword(), getName());
	}


	@Override
	public void setCredentialsMatcher(final CredentialsMatcher credentialsMatcher) {
		super.setCredentialsMatcher(credentialsMatcher);
	}

}
