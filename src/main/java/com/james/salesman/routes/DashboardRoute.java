package com.james.salesman.routes;

import com.james.salesman.iservice.IScene;

public enum DashboardRoute implements IScene {
	MAIN{
		@Override
		public String getTitle() {
			return "WELCOME DASHBOARD  - SalesMan v1.21";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/dashboard/main.fxml";
		}

	},

	DASHBOARD{
		@Override
		public String getTitle() {
			return "Dashboard  - SalesMan v1.21";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/dashboard/dashboard.fxml";
		}

	};

}
