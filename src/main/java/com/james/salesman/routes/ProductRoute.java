package com.james.salesman.routes;

import com.james.salesman.iservice.IScene;

public enum ProductRoute implements IScene {

	MAIN{
		@Override
		public String getTitle() {
			return "Products  - SalesMan v1.21";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/product/main.fxml";
		}

	},
	PRODUCT_ADD{
		@Override
		public String getTitle() {
			return "Add Products  - SalesMan v1.21";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/product/product/product-add.fxml";
		}

	},
	PRODUCT_LIST{
		@Override
		public String getTitle() {
			return "List Products  - SalesMan v1.21";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/product/product/product-list.fxml";
		}

	},
	PRODUCT_UPDATE{
		@Override
		public String getTitle() {
			return "Update Product  - SalesMan v1.21";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/product/product/product-update.fxml";
		}

	};

}
