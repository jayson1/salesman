package com.james.salesman.routes;

import com.james.salesman.iservice.IScene;

public enum GlobalSettingRoute implements IScene {

	GLOBAL_SETTING_MAIN {
		@Override
		public String getTitle() {
			return "Global Setting";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/global-setting/main.fxml";
		}

	},
	GLOBAL_SETTING_APPEARANCE {
		@Override
		public String getTitle() {
			return "Appearance Settings";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/global-setting/global_setting_appearance.fxml";
		}

	},
	GLOBAL_SETTING_DASHBOARD {
		@Override
		public String getTitle() {
			return "Dashboard Settings";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/global-setting/global_setting_dashboard.fxml";
		}

	},
	GLOBAL_SETTING_PRODUCT_SERVICE {
		@Override
		public String getTitle() {
			return "Products & Services Settings";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/global-setting/global_setting_product_service.fxml";
		}

	},
	GLOBAL_SETTING_STOCK_INVENTORY {
		@Override
		public String getTitle() {
			return "Stock & Inventory Settings";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/global-setting/global_setting_stock_inventory.fxml";
		}

	},
	GLOBAL_SETTING_RETURN_EXCHANGE {
		@Override
		public String getTitle() {
			return "Stock & Inventory Settings";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/global-setting/global_setting_return_exchange.fxml";
		}

	},
	GLOBAL_SETTING_EMAIL {
		@Override
		public String getTitle() {
			return "Email Settings";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/global-setting/global_setting_email.fxml";
		}

	},
	GLOBAL_SETTING_PAYMENT {
		@Override
		public String getTitle() {
			return "Payment Settings";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/global-setting/global_setting_payment.fxml";
		}

	},
	GLOBAL_SETTING_TAX_VAT {
		@Override
		public String getTitle() {
			return "Tax Settings";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/global-setting/global_setting_tax_vat.fxml";
		}

	},
	GLOBAL_SETTING_EXPENSES {
		@Override
		public String getTitle() {
			return "Expenses Settings";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/global-setting/global_setting_expenses.fxml";
		}

	},
	GLOBAL_SETTING_PRINT {
		@Override
		public String getTitle() {
			return "Print Settings";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/global-setting/global_setting_print.fxml";
		}

	},
}
