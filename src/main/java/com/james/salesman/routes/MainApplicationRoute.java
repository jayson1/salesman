package com.james.salesman.routes;

import com.james.salesman.iservice.IScene;

public enum MainApplicationRoute implements IScene {

	ADMIN_MAIN {
		@Override
		public String getTitle() {
			return "Management Section";
		}

		@Override
		public String getFXML() {
			return "/fxml/admin/main.fxml";
		}

	},

	LOGIN{
		@Override
		public String getTitle() {
			return "Login ";
		}

		@Override
		public String getFXML() {
			return "/fxml/auth/login.fxml";
		}
	},

	WELCOME{
		@Override
		public String getTitle() {
			return "Welcome";
		}

		@Override
		public String getFXML() {
			return "/fxml/welcome/welcome.fxml";
		}
	}

	/*
	public enum Product implements IScene {
		CREATE_PRODUCT{
			@Override
			public String getTitle() {
				return "Create Products";
			}

			@Override
			public String getFXML() {
				return "/fxml/products";
			}
		}
	}
	 */

}
