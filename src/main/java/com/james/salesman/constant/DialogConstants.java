package com.james.salesman.constant;

public class DialogConstants {
	public static final String POWER_DIALOG_TITLE = "Choose Your Action";
	public static final String LOGIN_ERROR_MESSAGE = "Email and Password combination is incorrect";
	public static final String LOGIN_ERROR_DIALOG_TITLE = "Warning";
	public static final String CONFIRM_TEXT = "Are you sure?";
	public static final String WARNING_TEXT = "Warning!";
	public static final String BTN_LOGOUT_TEXT = "Logout";
	public static final String BTN_LOGOUT_CONFIRM_TEXT = "Yes, Logout!";
	public static final String BTN_EXIT_TEXT = "Exit";
	public static final String BTN_EXIT_CONFIRM_TEXT = "Yes, Exit!";
	public static final String BTN_SHUTDOWN_TEXT = "Shutdown";

	public static final String SPINNER_TEXT = "PLEASE WAIT";

	public static final String PRIMARY_FILL_STYLE = "-fx-fill: '#128C7E'";
	public static final String WHITE_FILL_STYLE = "-fx-fill: '#FFFFFF'";

	public static final String PRIMARY_ACTIVE_USER_TOP_LABEL_STYLE = "-fx-text-fill: #212936; -fx-font-family: Roboto; -fx-font-size: 14px;";
	public static final String PRIMARY_ACTIVE_USER_BOTTOM_LABEL_STYLE = "-fx-text-fill: #C0C0C0; -fx-font-family: Roboto; -fx-font-size: 12px;";

	public static final String WHITE_ACTIVE_USER_TOP_LABEL_STYLE = "-fx-text-fill: white; -fx-font-family: Roboto; -fx-font-size: 14px;";
	public static final String WHITE_ACTIVE_USER_BOTTOM_LABEL_STYLE_ = "-fx-text-fill: white; -fx-font-family: Roboto; -fx-font-size: 12px;";

	public static final String CSS_USER_FULL_NAME = "welcome-fullname";

}
