package com.james.salesman.constant;

public enum GenderEnum {
	MALE("MALE"),
	FEMALE("FEMALE");

	private final String label;

	GenderEnum(String label) {
		this.label = label;
	}
}
