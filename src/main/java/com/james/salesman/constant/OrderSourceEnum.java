package com.james.salesman.constant;

public enum  OrderSourceEnum {
	IN_APP("IN APP"),
	ONLINE("ONLINE"),
	BRANCH("BRANCH");

	private final String label;

	OrderSourceEnum(String label){
		this.label = label;
	}
}
