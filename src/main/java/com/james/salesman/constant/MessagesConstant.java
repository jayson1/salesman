package com.james.salesman.constant;

public class MessagesConstant {
	public static final String SEED_STARTED = "STARTING DATABASE SEEDING...";
	public static final String ALREADY_SEEDED = "DATABASE IS ALREADY SEEDED, OPERATION ABORTED";
	public static final String SEEDED_SUCCESSFULLY = "DATABASE SEEDED SUCCESSFULLY";
	public static final String EMPTY_PERMISSION_ERROR = "CANNOT SEED EMPTY LIST, OPERATION ABORTED";
	public static final String SEED_FAILED = "OPERATION FAILED";

	public static final String SEEDING_ROLE = "SEEDING DEFAULT ROLE";
	public static final String SEEDING_ROLE_SUCCESS = "SEEDED DEFAULT ROLE SUCCESSFULLY";

	public static final String NOT_AVAILABLE = "N/A";
}
