package com.james.salesman.constant;

import static com.james.salesman.constant.GlobalSettingConstant.*;
import static com.james.salesman.constant.ModuleConstant.*;

public enum GlobalSettingDefaultEnum {

	E_ENABLE_PRODUCT_VARIATION {
		@Override
		public String key() {
			return ENABLE_PRODUCT_VARIATION;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return PRODUCT;
		}
	},
	E_ENABLE_PRICE_TAG {
		@Override
		public String key() {
			return ENABLE_PRICE_TAG;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return PRODUCT;
		}
	},
	E_DEACTIVATE_SERVICES {
		@Override
		public String key() {
			return DEACTIVATE_SERVICES;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return SERVICE;
		}
	},
	E_APPLY_DISCOUNT_BEFORE_TAX {
		@Override
		public String key() {
			return APPLY_DISCOUNT_BEFORE_TAX;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return DISCOUNT;
		}
	},
	E_APPLY_DISCOUNT_AFTER_TAX {
		@Override
		public String key() {
			return APPLY_DISCOUNT_AFTER_TAX;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return DISCOUNT;
		}
	},
	E_ENABLE_PRODUCT_BARCODE {
		@Override
		public String key() {
			return ENABLE_PRODUCT_BARCODE;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return PRODUCT;
		}
	},
	E_ENABLE_DISCOUNT_ON_SINGLE_SERVICE {
		@Override
		public String key() {
			return ENABLE_DISCOUNT_ON_SINGLE_SERVICE;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return SERVICE;
		}
	},
	E_ENABLE_DISCOUNT_ON_SINGLE_PRODUCT {
		@Override
		public String key() {
			return ENABLE_DISCOUNT_ON_SINGLE_PRODUCT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return PRODUCT;
		}
	},
	E_ALLOW_CREDIT_PAYMENT_ON_SINGLE_PRODUCT {
		@Override
		public String key() {
			return ALLOW_CREDIT_PAYMENT_ON_SINGLE_PRODUCT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return PRODUCT;
		}
	},
	E_ALLOW_CREDIT_PAYMENT_ON_SINGLE_SERVICE {
		@Override
		public String key() {
			return ALLOW_CREDIT_PAYMENT_ON_SINGLE_SERVICE;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return SERVICE;
		}
	},
	E_ENABLE_STOCK_AND_INVENTORY {
		@Override
		public String key() {
			return ENABLE_STOCK_AND_INVENTORY;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return STOCK_INVENTORY;
		}
	},
	E_DISABLE_CUSTOMER_POINT {
		@Override
		public String key() {
			return DISABLE_CUSTOMER_POINT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return CUSTOMER;
		}
	},
	E_ENABLE_CUSTOMER_POINTS_FOR_DISCOUNT {
		@Override
		public String key() {
			return ENABLE_CUSTOMER_POINTS_FOR_DISCOUNT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return CUSTOMER;
		}
	},
	E_ENABLE_PRODUCT_EXCHANGE {
		@Override
		public String key() {
			return ENABLE_PRODUCT_EXCHANGE;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return PRODUCT;
		}
	},
	E_ENABLE_PRODUCT_COST_PRICE_UPDATE_ON_RESTOCK {
		@Override
		public String key() {
			return ENABLE_PRODUCT_COST_PRICE_UPDATE_ON_RESTOCK;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return PRODUCT;
		}
	},
	E_ENABLE_TAX_AND_VAT {
		@Override
		public String key() {
			return ENABLE_TAX_AND_VAT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return TAX_VAT;
		}
	},
	E_SET_EXPENSE_LIMIT {
		@Override
		public String key() {
			return SET_EXPENSE_LIMIT;
		}

		@Override
		public String value() {
			return "null";
		}

		@Override
		public String section() {
			return EXPENSES;
		}
	},
	E_ENABLE_SUPPLIERS_PAYMENT_STATUS {
		@Override
		public String key() {
			return ENABLE_SUPPLIERS_PAYMENT_STATUS;
		}

		@Override
		public String value() {
			return String.valueOf(true);
		}

		@Override
		public String section() {
			return SUPPLIER;
		}
	},
	E_ENABLE_CUSTOMERS_PAYMENT_STATUS {
		@Override
		public String key() {
			return ENABLE_CUSTOMERS_PAYMENT_STATUS;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return CUSTOMER;
		}
	},
	E_SET_PRODUCT_GLOBAL_MARKUP {
		@Override
		public String key() {
			return SET_PRODUCT_GLOBAL_MARKUP;
		}

		@Override
		public String value() {
			return "null";
		}

		@Override
		public String section() {
			return PRODUCT;
		}
	},
	E_SET_RECEIPT_COPY_COUNT {
		@Override
		public String key() {
			return SET_RECEIPT_COPY_COUNT;
		}

		@Override
		public String value() {
			return "null";
		}

		@Override
		public String section() {
			return PRINT_SETTING;
		}
	},
	E_SET_PRINT_FONT {
		@Override
		public String key() {
			return SET_PRINT_FONT;
		}

		@Override
		public String value() {
			return "null";
		}

		@Override
		public String section() {
			return PRINT_SETTING;
		}
	},
	E_SHOW_UNIT_OF_MEASURE_ON_RECEIPT {
		@Override
		public String key() {
			return SHOW_UNIT_OF_MEASURE_ON_RECEIPT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return RECEIPT_SETTING;
		}
	},
	E_SHOW_ITEMS_COUNT_ON_RECEIPT {
		@Override
		public String key() {
			return SHOW_ITEMS_COUNT_ON_RECEIPT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return RECEIPT_SETTING;
		}
	},
	E_SHOW_CUSTOMER_NAME_ON_RECEIPT {
		@Override
		public String key() {
			return SHOW_CUSTOMER_NAME_ON_RECEIPT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return RECEIPT_SETTING;
		}
	},
	E_SHOW_CUSTOMER_EMAIL_ON_RECEIPT {
		@Override
		public String key() {
			return SHOW_CUSTOMER_EMAIL_ON_RECEIPT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return RECEIPT_SETTING;
		}
	},
	E_SHOW_CUSTOMER_PHONE_NUMBER_ON_RECEIPT {
		@Override
		public String key() {
			return SHOW_CUSTOMER_PHONE_NUMBER_ON_RECEIPT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return RECEIPT_SETTING;
		}
	},
	E_SHOW_CUSTOMER_ADDRESS_ON_RECEIPT {
		@Override
		public String key() {
			return SHOW_CUSTOMER_ADDRESS_ON_RECEIPT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return RECEIPT_SETTING;
		}
	},
	E_SHOW_CUSTOMER_LOYALTY_CARD_NUMBER_ON_RECEIPT {
		@Override
		public String key() {
			return SHOW_CUSTOMER_LOYALTY_CARD_NUMBER_ON_RECEIPT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return RECEIPT_SETTING;
		}
	},
	E_SHOW_COMPANY_ADDRESS_ON_RECEIPT {
		@Override
		public String key() {
			return SHOW_COMPANY_ADDRESS_ON_RECEIPT;
		}

		@Override
		public String value() {
			return String.valueOf(true);
		}

		@Override
		public String section() {
			return RECEIPT_SETTING;
		}
	},
	E_SHOW_COMPANY_PHONE_NUMBER_ON_RECEIPT {
		@Override
		public String key() {
			return SHOW_COMPANY_PHONE_NUMBER_ON_RECEIPT;
		}

		@Override
		public String value() {
			return String.valueOf(true);
		}

		@Override
		public String section() {
			return RECEIPT_SETTING;
		}
	},
	E_SET_EMAIL_USERNAME {
		@Override
		public String key() {
			return SET_EMAIL_USERNAME;
		}

		@Override
		public String value() {
			return "null";
		}

		@Override
		public String section() {
			return EMAIL_SETTING;
		}
	},
	E_SET_EMAIL_HOST {
		@Override
		public String key() {
			return SET_EMAIL_HOST;
		}

		@Override
		public String value() {
			return "null";
		}

		@Override
		public String section() {
			return EMAIL_SETTING;
		}
	},
	E_SET_EMAIL_PASSWORD {
		@Override
		public String key() {
			return SET_EMAIL_PASSWORD;
		}

		@Override
		public String value() {
			return "null";
		}

		@Override
		public String section() {
			return EMAIL_SETTING;
		}
	},
	E_ENABLE_USER_ACCOUNT_UPDATE {
		@Override
		public String key() {
			return ENABLE_USER_ACCOUNT_UPDATE;
		}

		@Override
		public String value() {
			return String.valueOf(true);
		}

		@Override
		public String section() {
			return USER_SETTING;
		}
	},
	E_ACTIVATE_APPLICATION_AUTO_UPDATE {
		@Override
		public String key() {
			return ACTIVATE_APPLICATION_AUTO_UPDATE;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return APPLICATION_SETTING;
		}
	},
	E_SHOW_TRENDING_PRODUCT {
		@Override
		public String key() {
			return SHOW_TRENDING_PRODUCT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_HIGHEST_PRODUCT_CATEGORY {
		@Override
		public String key() {
			return SHOW_HIGHEST_PRODUCT_CATEGORY;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_TOP_CUSTOMER {
		@Override
		public String key() {
			return SHOW_TOP_CUSTOMER;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_TOP_EMPLOYEE {
		@Override
		public String key() {
			return SHOW_TOP_EMPLOYEE;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_TOP_SUPPLIER {
		@Override
		public String key() {
			return SHOW_TOP_SUPPLIER;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_HOURLY_SALES_REPORT {
		@Override
		public String key() {
			return SHOW_HOURLY_SALES_REPORT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_MONTHLY_SALES {
		@Override
		public String key() {
			return SHOW_MONTHLY_SALES;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_YEARLY_SALES {
		@Override
		public String key() {
			return SHOW_YEARLY_SALES;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_TOP_PURCHASED {
		@Override
		public String key() {
			return SHOW_TOP_PURCHASED;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_HIGHEST_STOCK {
		@Override
		public String key() {
			return SHOW_HIGHEST_STOCK;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_LOWEST_STOCK_PRODUCT {
		@Override
		public String key() {
			return SHOW_LOWEST_STOCK_PRODUCT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_HIGHEST_STOCK_PRODUCT {
		@Override
		public String key() {
			return SHOW_HIGHEST_STOCK_PRODUCT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_HIGHEST_RETURNED_PRODUCT {
		@Override
		public String key() {
			return SHOW_HIGHEST_RETURNED_PRODUCT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_HIGHEST_EXCHANGED_PRODUCT {
		@Override
		public String key() {
			return SHOW_HIGHEST_EXCHANGED_PRODUCT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_NON_TRENDING_PRODUCT {
		@Override
		public String key() {
			return SHOW_NON_TRENDING_PRODUCT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_OUT_OF_STOCK_PRODUCT {
		@Override
		public String key() {
			return SHOW_OUT_OF_STOCK_PRODUCT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_EXPIRING_PRODUCT {
		@Override
		public String key() {
			return SHOW_EXPIRING_PRODUCT;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	},
	E_SHOW_TOP_SUPPLIERS {
		@Override
		public String key() {
			return SHOW_TOP_SUPPLIERS;
		}

		@Override
		public String value() {
			return String.valueOf(false);
		}

		@Override
		public String section() {
			return GLOBAL_SETTING;
		}
	};

	public abstract String key();

	public abstract String value();

	public abstract String section();
}
