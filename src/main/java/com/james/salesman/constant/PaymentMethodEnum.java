package com.james.salesman.constant;

public enum PaymentMethodEnum {
	CASH,
	POS,
	TRANSFER,
	CHEQUE
}
