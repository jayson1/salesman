package com.james.salesman.constant;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.james.salesman.constant.PermissionsConstant.*;

public enum PermissionsEnum  {

	PRODUCT{
		@Override
		public String section() {
			return PRODUCT_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			 ACCESS_PRODUCT_VIEW,
			 ACCESS_PRODUCT_CREATE,
			 ACCESS_PRODUCT_UPDATE,
			 ACCESS_PRODUCT_IMPORT,
			 ACCESS_PRODUCT_EXPORT,
			 ACCESS_PRODUCT_DISABLE,
			 ACCESS_PRODUCT_ENABLE,
			 ACCESS_PRODUCT_LIST_PRINTING,
			 ACCESS_PRODUCT_PRICE_TAG,
			 ACCESS_PRODUCT_BROWSING,
			 ACCESS_PRODUCT_UPLOADING,
			 ACCESS_PRODUCT_DOWNLOAD);
		}
	},

	CATEGORY{
		@Override
		public String section() {
			return PRODUCT_CATEGORY_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			 ACCESS_PRODUCT_CATEGORY_VIEW,
			 ACCESS_PRODUCT_CATEGORY_CREATE,
			 ACCESS_PRODUCT_CATEGORY_UPDATE,
			 ACCESS_PRODUCT_CATEGORY_IMPORT,
			 ACCESS_PRODUCT_CATEGORY_EXPORT,
			 ACCESS_PRODUCT_CATEGORY_DISABLE,
			 ACCESS_PRODUCT_CATEGORY_ENABLE);
		}
	},

	CUSTOMER{
		@Override
		public String section(){
			return CUSTOMER_SECTION;
		}

		@Override
		public List<String> permissions(){
			return Arrays.asList(
			 ACCESS_CUSTOMER_CREATE,
			 ACCESS_CUSTOMER_LIST_VIEW,
			 ACCESS_CUSTOMER_UPDATE,
			 ACCESS_CUSTOMER_DETAILS_VIEW,
			 ACCESS_CUSTOMER_POINT_ACTIVATION,
			 ACCESS_CUSTOMER_POINT_DEACTIVATION,
			 ACCESS_CUSTOMER_POINT_VIEW,
			 ACCESS_CUSTOMER_POINT_HISTORY,
			 ACCESS_CUSTOMER_UPLOAD,
			 ACCESS_CUSTOMER_DOWNLOAD,
			 ACCESS_CUSTOMER_DOWNLOAD_SALES_HISTORY);
		}
	},

	SUPPLIER{
		@Override
		public String section() {
			return SUPPLIER_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			 ACCESS_SUPPLIER_CREATE,
			 ACCESS_SUPPLIER_VIEW,
			 ACCESS_SUPPLIER_UPDATE,
			 ACCESS_SUPPLIER_DETAILS_UPDATE,
			 ACCESS_SUPPLIER_DETAILS_VIEW,
			 ACCESS_SUPPLIER_ENABLE,
			 ACCESS_SUPPLIER_DISABLE,
			 ACCESS_SUPPLIER_PRODUCT_HISTORY,
			 ACCESS_SUPPLIER_UPLOAD,
			 ACCESS_SUPPLIER_DOWNLOAD);
		}
	},

	PROMOTION{
		@Override
		public String section() {
			return PROMOTION_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			  ACCESS_PROMOTION_CREATE,
			  ACCESS_PROMOTION_UPDATE,
			  ACCESS_PROMOTION_VIEW,
			  ACCESS_PROMOTION_DISABLE,
			  ACCESS_PROMOTION_REACTIVATION,
			  ACCESS_PROMOTION_DEACTIVATION);
		}
	},

	DISCOUNT{
		@Override
		public String section() {
			return DISCOUNT_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
					ACCESS_DISCOUNT_CREATE,
					ACCESS_DISCOUNT_UPDATE,
					ACCESS_DISCOUNT_VIEW,
					ACCESS_DISCOUNT_DISABLE,
					ACCESS_DISCOUNT_SET_STRATEGY,
					ACCESS_DISCOUNT_REACTIVATION,
					ACCESS_DISCOUNT_DEACTIVATION,
					ACCESS_CUSTOMER_PRODUCT_DISCOUNT);
		}
	},

	RETURN{
		@Override
		public String section() {
			return RETURN_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			  ACCESS_RETURN_PRODUCT,
			  ACCESS_RETURN_PRODUCT_HISTORY);
		}
	},

	EXCHANGE{
		@Override
		public String section() {
			return EXCHANGE_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			ACCESS_EXCHANGE_PRODUCT,
			ACCESS_EXCHANGE_PRODUCT_HISTORY);
		}
	},

	ROLE{
		@Override
		public String section() {
			return ROLE_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			ACCESS_ROLE_CREATE,
			ACCESS_ROLE_UPDATE,
			ACCESS_ROLE_VIEW,
			ACCESS_ROLE_DISABLE,
			ACCESS_ROLE_ENABLE);
		}
	},

	DEPARTMENT{
		@Override
		public String section() {
			return DEPARTMENT_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			  ACCESS_DEPARTMENT_CREATE,
			  ACCESS_DEPARTMENT_UPDATE,
			  ACCESS_DEPARTMENT_VIEW,
			  ACCESS_DEPARTMENT_ENABLE,
			  ACCESS_DEPARTMENT_DISABLE,
			  ACCESS_DEPARTMENT_UPLOAD,
			  ACCESS_DEPARTMENT_DOWNLOAD);
		}
	},

	STAFF{
		@Override
		public String section() {
			return STAFF_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			  ACCESS_STAFF_CREATE,
			  ACCESS_STAFF_UPDATE,
			  ACCESS_STAFF_DISABLE,
			  ACCESS_STAFF_ENABLE,
			  ACCESS_STAFF_UPLOAD,
			  ACCESS_STAFF_DOWNLOAD,
			  ACCESS_STAFF_HISTORY);
		}
	},

	USER{
		@Override
		public String section() {
			return USER_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			 ACCESS_USER_CREATE,
			 ACCESS_USER_VIEW,
			  ACCESS_USER_ASSIGN_STAFF,
			  ACCESS_USER_DISABLE,
			  ACCESS_USER_ENABLE,
			  ACCESS_USER_DOWNLOAD,
			  ACCESS_USER_UPLOAD,
			  ACCESS_USER_PERMISSION,
			  ACCESS_USER_ROLE);
		}
	},

	CABINET {
		@Override
		public String section() {
			return CABINET_SECTION;
		}
		@Override
		public List< String > permissions() {
			return  Arrays.asList(
			 ACCESS_CABINET_CREATE,
			 ACCESS_CABINET_UPDATE,
			 ACCESS_CABINET_ENABLE,
			 ACCESS_CABINET_DISABLE,
			 ACCESS_CABINET_UPLOAD,
			 ACCESS_CABINET_DOWNLOAD,
			 ACCESS_CABINET_VIEW);
		}
	},

	TAX_VAT{
		@Override
		public String section() {
			return TAX_VAT_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			  ACCESS_TAX_VAT_CREATE,
			  ACCESS_TAX_VAT_UPDATE,
			  ACCESS_TAX_VAT_ENABLE,
			  ACCESS_TAX_VAT_DISABLE,
			  ACCESS_TAX_VAT_VIEW);
		}
	},
	EXPENSES{
		@Override
		public String section() {
			return EXPENSES_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			  ACCESS_EXPENSES_CREATE,
			  ACCESS_EXPENSES_UPDATE,
			  ACCESS_EXPENSES_VIEW,
			  ACCESS_EXPENSES_DELETE);
		}
	},

	COMPANY{
		@Override
		public String section() {
			return COMPANY_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			  ACCESS_COMPANY_VIEW,
			  ACCESS_COMPANY_UPDATE);
		}
	},

	REPORT{
		@Override
		public String section() {
			return REPORT_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			  ACCESS_REPORT_GENERATE,
			  ACCESS_REPORT_VIEW,
			  ACCESS_REPORT_SALES,
			  ACCESS_REPORT_INVENTORY,
			  ACCESS_REPORT_INVENTORY_ITEM_HISTORY,
			  ACCESS_REPORT_MERGING,
			  ACCESS_REPORT_DOWNLOAD,
			  ACCESS_REPORT_FILTERING);
		}
	},

	GLOBAL_SETTING{
		@Override
		public String section() {
			return GLOBAL_SETTING_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Collections.singletonList(ACCESS_GLOBAL_SETTING_VIEW);
		}
	},

	STOCK{
		@Override
		public String section() {
			return STOCK_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			  ACCESS_STOCK_VIEW,
			  ACCESS_STOCK_CREATE,
			  ACCESS_STOCK_UPDATE,
			  ACCESS_STOCK_RECONCILE,
			  ACCESS_STOCK_DISABLE,
			  ACCESS_STOCK_ENABLE,
			  ACCESS_STOCK_PUSH_TO_LOCATION);
		}
	},

	INVENTORY{
		@Override
		public String section() {
			return INVENTORY_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			  ACCESS_INVENTORY_VIEW,
			  ACCESS_INVENTORY_WRITE_OFF,
			  ACCESS_INVENTORY_PUSHING,
			  ACCESS_INVENTORY_DOWNLOAD,
			  ACCESS_INVENTORY_ITEM_HISTORY);
		}
	},

	LOCATION{
		@Override
		public String section() {
			return LOCATION_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			 ACCESS_LOCATION_CREATE,
			 ACCESS_LOCATION_UPDATE,
			 ACCESS_LOCATION_DISABLE,
			 ACCESS_LOCATION_ENABLE,
			 ACCESS_LOCATION_DOWNLOAD,
			 ACCESS_LOCATION_UPLOAD);
		}
	},

	SERVICE{
		@Override
		public String section() {
			return SERVICE_SECTION;
		}

		@Override
		public List< String > permissions() {
			return Arrays.asList(
			  ACCESS_SERVICE_CREATE,
			  ACCESS_SERVICE_UPDATE,
			  ACCESS_SERVICE_VIEW,
			  ACCESS_SERVICE_DISABLE,
			  ACCESS_SERVICE_ENABLE,
			  ACCESS_SERVICE_UPLOAD,
			  ACCESS_SERVICE_DOWNLOAD);
		}
	}
	;


	public abstract String section();
	public abstract List<String> permissions();

}
