package com.james.salesman.constant;

public enum DiscountTypeEnum {
	PERCENTAGE("PERCENTAGE"),
	AMOUNT("AMOUNT");

	private final String label;

	DiscountTypeEnum(String label) {
		this.label = label;
	}
}
