package com.james.salesman.constant;

public enum  OrderDeliveryOptionEnum {
	DELIVERY("DELIVERY"),
	PICK_UP("PICK UP"),
	DROP_OFF("DROP OFF");

	private final String label;

	OrderDeliveryOptionEnum(String label){
		this.label  = label;
	}
}
