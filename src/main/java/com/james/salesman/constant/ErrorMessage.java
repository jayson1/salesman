package com.james.salesman.constant;

public class ErrorMessage {
	public static final String USERNAME_REQUIRED = "Username is required";
	public static final String INVALID_LOGIN_CREDENTIALS = "Invalid Login Credentials";
	public static final String TOKEN_MISMATCH = "Token has to be instance of UsernamePasswordToken";
	public static final String LOGIN_FAILED = "Login Failed";
}
