package com.james.salesman.constant;

public enum ModuleEnum {
	DASHBOARD {
		@Override
		public String idValue() {
			return "#dashboardNavWrapper";
		}

		@Override
		public String moduleTitle() {
			return "Dashboard";
		}
	},
	PRODUCT_AND_SERVICE {
		@Override
		public String idValue() {
			return "#productNavWrapper";
		}

		@Override
		public String moduleTitle() {
			return "Products & Services";
		}
	},
	STOCK_AND_INVENTORY {
		@Override
		public String idValue() {
			return "#stockNavWrapper";
		}

		@Override
		public String moduleTitle() {
			return "Stock & Inventory";
		}
	},
	CUSTOMERS_AND_SUPPLIERS {
		@Override
		public String idValue() {
			return "#customersNavWrapper";
		}

		@Override
		public String moduleTitle() {
			return "Customers & Suppliers";
		}
	},
	DISCOUNT_AND_PROMOTION {
		@Override
		public String idValue() {
			return "#discountNavWrapper";
		}

		@Override
		public String moduleTitle() {
			return "Discount & Promotion";
		}
	},
	RETURN_AND_EXCHANGE {
		@Override
		public String idValue() {
			return "#returnNavWrapper";
		}

		@Override
		public String moduleTitle() {
			return "Return & Exchange";
		}
	},
	COMPANY {
		@Override
		public String idValue() {
			return "#companyNavWrapper";
		}

		@Override
		public String moduleTitle() {
			return "Company";
		}
	},
	REPORTS {
		@Override
		public String idValue() {
			return "#reportsNavWrapper";
		}

		@Override
		public String moduleTitle() {
			return "Reports";
		}
	},
	GLOBAL_SETTING {
		@Override
		public String idValue() {
			return "#globalSetNavWrapper";
		}

		@Override
		public String moduleTitle() {
			return "Global Setting";
		}
	},
	HELP_INFORMATION {
		@Override
		public String idValue() {
			return "#helpInfoWrapper";
		}

		@Override
		public String moduleTitle() {
			return "Help & Information";
		}
	};

	public abstract String idValue();
	public abstract String moduleTitle();

}
