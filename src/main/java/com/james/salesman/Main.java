package com.james.salesman;

import com.james.salesman.dto.SceneInfoDto;
import com.james.salesman.routes.MainApplicationRoute;
import com.james.salesman.utils.StageManager;
import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Main extends Application {

	private ConfigurableApplicationContext springContext;
	private StageManager stageManager;

	public static void main(String[] args) {
		Application.launch( args);
	}

	@Override
	public void start(Stage stage) {
		this.stageManager = this.springContext.getBean(StageManager.class, stage);
		this.displayInitialScene();
	}

	@Override
	public void init() {
		this.springContext = bootstrapSpringApplicationContext();
	}

	@Override
	public void stop() {
		this.springContext.close();
	}

	protected void displayInitialScene(){
		this.stageManager.switchScene(MainApplicationRoute.LOGIN, new SceneInfoDto(true));
	}

	private ConfigurableApplicationContext bootstrapSpringApplicationContext(){
		SpringApplicationBuilder builder = new SpringApplicationBuilder(Main.class);
		String[] args = getParameters().getRaw().toArray(new String[0]);
		return builder.run(args);
	}
}
