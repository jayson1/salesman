package com.james.salesman.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;


@Data @ToString @AllArgsConstructor @NoArgsConstructor
public class Response<T> {
	private boolean status = false;
	private List<String> messages = new ArrayList<>();
	private T data = null;
}
