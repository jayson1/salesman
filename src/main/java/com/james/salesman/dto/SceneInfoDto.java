package com.james.salesman.dto;

import com.james.salesman.constant.ModuleEnum;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data @ToString @AllArgsConstructor @NoArgsConstructor
public class SceneInfoDto {
	private int minWidth = 680;
	private int minHeight = 580;
	private Boolean maximized =  false;
	private List<String> breadCrumbs =  new ArrayList<>();
	private ModuleEnum activeModule;


	public SceneInfoDto(Boolean maximized, List< String > breadCrumbs, ModuleEnum activeModule) {
		this.maximized = maximized;
		this.breadCrumbs = breadCrumbs;
		this.activeModule = activeModule;
	}

	public SceneInfoDto(Boolean maximized) {
		this.maximized = maximized;
	}
}
