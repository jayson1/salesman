package com.james.salesman.dto;

import lombok.Data;
import org.apache.shiro.util.ByteSource;

@Data
public class HashDto {
	private String password;
	private ByteSource salt;
}
