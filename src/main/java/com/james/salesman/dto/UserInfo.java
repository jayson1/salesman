package com.james.salesman.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data @AllArgsConstructor
public class UserInfo {
	private String fullName;
	private String username;
}
