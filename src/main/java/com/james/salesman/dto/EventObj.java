package com.james.salesman.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EventObj {
	private String message;
	private Object newObj;
}
