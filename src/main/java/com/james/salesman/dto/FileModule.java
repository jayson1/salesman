package com.james.salesman.dto;

import com.james.salesman.constant.FileFormatEnum;
import com.james.salesman.constant.FileModuleEnum;
import com.james.salesman.constant.OperationStatusEnum;
import com.james.salesman.constant.OperationTypeEnum;
import com.james.salesman.model.User;

public class FileModule {
	private FileModuleEnum fileModule;
	private OperationTypeEnum operationType;
	private FileFormatEnum fileFormatType;
	private User requestUser;
	private OperationStatusEnum operationStatus;
}
