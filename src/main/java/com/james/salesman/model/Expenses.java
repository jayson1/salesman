package com.james.salesman.model;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table(name = TABLE_PREFIX+EXPENSES)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@ToString
public class Expenses extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = EXPENSES)
	@SequenceGenerator(name = EXPENSES, sequenceName = SEQUENCE_PREFIX+EXPENSES)
	private Long id;

	@Column(name = "amount", nullable =  false)
	private Double amount;

	@Column(name = "title", nullable =  false)
	private String title;

	@Column(name = "note")
	private String note;

	@OneToOne
	@JoinColumn(name = "expenses_category_id")
	private ExpensesCategory expensesCategory;

	@OneToOne
	@JoinColumn(name = "staff_id")
	private Staff staff;

	@Column(name = "date", nullable =  false)
	private LocalDate date;

}
