package com.james.salesman.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+ORDER_ITEM)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class OrderItem extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ORDER_ITEM)
	@SequenceGenerator(name = ORDER_ITEM, sequenceName = SEQUENCE_PREFIX+ORDER_ITEM)
	private Long id;

	@OneToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@Column(name = "quantity", nullable = false)
	private Integer quantity;

	@Column(name = "net_amount", nullable = false)
	@ColumnDefault("0.00")
	private Double netAmount;

	@Column(name = "discount_amount", nullable = false)
	@ColumnDefault("0.00")
	private Double discountAmount;

	@Column(name = "gross_amount", nullable = false)
	@ColumnDefault("0.00")
	private Double gross;

	@ManyToOne
	@JoinColumn(name = "order_id", nullable = false)
	private Order order;

}
