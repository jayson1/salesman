package com.james.salesman.model;

import com.james.salesman.constant.GenderEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table(name = TABLE_PREFIX+CUSTOMER)
@EqualsAndHashCode(callSuper = true)
@Data @ToString @NoArgsConstructor
public class Customer extends Base {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = CUSTOMER)
	@SequenceGenerator(name = CUSTOMER, sequenceName = SEQUENCE_PREFIX+CUSTOMER)
	private Long id;

	@Column(name = "nickname", nullable =  false)
	private String nickname;

	@Column(name = "email")
	private String email;

	@Column(name = "phone")
	private String phone;

	@Column(name = "level")
	private String level;

//	@Column(name = "points", columnDefinition = "double precision default '0.00'")
	@Column(name = "points")
	@ColumnDefault("0.00")
	private Double points;

	@Column(name = "uuid")
	private String uuid;

	@Column(name = "gender")
	private GenderEnum gender;


}
