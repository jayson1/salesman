package com.james.salesman.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+UNIT_CONVERSION)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class UnitConversion extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = UNIT_CONVERSION)
	@SequenceGenerator(name = UNIT_CONVERSION, sequenceName = SEQUENCE_PREFIX+UNIT_CONVERSION)
	private Long id;

	@OneToOne
	@JoinColumn(name = "from_left")
	private MeasurementUnit fromLeft;

	@OneToOne
	@JoinColumn(name = "from_right")
	private MeasurementUnit toRight;

	@Column(name = "from_left_value")
	private Double fromLeftValue;

	@Column(name = "to_right_value")
	private Double toRightValue;

	@Column(name = "status")
	private Boolean status;

}
