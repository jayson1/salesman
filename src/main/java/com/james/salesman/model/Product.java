package com.james.salesman.model;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDate;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+PRODUCT)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class Product extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PRODUCT)
	@SequenceGenerator(name = PRODUCT, sequenceName = SEQUENCE_PREFIX+PRODUCT)
	private Long id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "code", unique = true)
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "selling_price")
	@ColumnDefault("0.00")
	private Double sellingPrice;

	@Column(name = "cost_price")
	@ColumnDefault("0.00")
	private Double costPrice;

	@Lob
	@Type( type="org.hibernate.type.BinaryType")
	@Column(name = "image")
	private byte[] image;

	@Column(name = "margin")
	@ColumnDefault("0.00")
	private Double margin;

	@OneToOne
	@JoinColumn(name = "currency_id")
	private Currency currency;

	@OneToOne
	@JoinColumn(name = "category_id")
	private ProductCategory category;

	@OneToOne
	@JoinColumn(name = "barcode_id")
	private Barcode barcode;

	@Column(name = "is_tax_free")
	private Boolean isTaxFree;

	//allow user to add discount directly to the product
	@Column(name = "direct_discount")
	private Double directDiscount;

	@Column(name = "tax_level")
	private Double taxLevel;

	@Column(name = "quantity")
	private Integer quantity;

	@Column(name = "alert_quantity")
	private Integer alertQuantity;

	@Column(name = "expiry_date")
	private LocalDate expiryDate;

	@Column(name = "manufacturing_date")
	private LocalDate manufacturingDate;

	@OneToOne
	@JoinColumn(name = "cabinet")
	private Cabinet cabinet;

	@OneToOne
	@JoinColumn(name = "supplier")
	private Supplier supplier;

	@OneToOne
	@JoinColumn(name = "measurement_unit")
	private MeasurementUnit measurementUnit;

	@OneToOne
	@JoinColumn(name = "discount_id")
	private Discount discount;

}
