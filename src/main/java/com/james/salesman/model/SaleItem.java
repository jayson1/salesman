package com.james.salesman.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDate;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+SALE_ITEM)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class SaleItem extends Base {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SALE_ITEM)
	@SequenceGenerator(name = SALE_ITEM, sequenceName = SEQUENCE_PREFIX+SALE_ITEM)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "sale_id")
	private Sale sale;

	@OneToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@Column(name = "quantity")
	@ColumnDefault("0")
	private Integer quantity;

	@Column(name = "gross_amount")
	@ColumnDefault("0.00")
	private Double grossAmount;

	@Column(name = "net_amount")
	@ColumnDefault("0")
	private Double netAmount;

	@Column(name = "discount")
	@ColumnDefault("0.00")
	private Double discount;

	@Column(name = "date")
	private LocalDate date;

}
