package com.james.salesman.model;

import lombok.*;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table(name = TABLE_PREFIX+DEPARTMENT)
@EqualsAndHashCode(callSuper = true)
@ToString @Data @NoArgsConstructor
public class Department extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = DEPARTMENT)
	@SequenceGenerator(name = DEPARTMENT, sequenceName = SEQUENCE_PREFIX+DEPARTMENT)
	private  Long id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "code")
	private String code;

//	@OneToOne(mappedBy = "department", targetEntity = User.class, cascade = CascadeType.ALL)
//	private User user;


}
