package com.james.salesman.model;

import javax.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


import static com.james.salesman.constant.ModuleConstant.*;

@EqualsAndHashCode( callSuper = true )
@Entity
@Table( name = TABLE_PREFIX + COUNTRY )
@NoArgsConstructor
@ToString
public class Country extends Base {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = COUNTRY)
	@SequenceGenerator(name = COUNTRY, sequenceName = SEQUENCE_PREFIX+COUNTRY)
	private Long id;

	@Column(name = "title", nullable = false)
	private String title;

//	@OneToMany(mappedBy = "country")
//	private List<CountryState> states;
}
