package com.james.salesman.model;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+SOCIAL_MEDIA)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class SocialMedia extends Base{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SOCIAL_MEDIA)
	@SequenceGenerator(name = SOCIAL_MEDIA, sequenceName = SEQUENCE_PREFIX+SOCIAL_MEDIA)
	private Long id;

	@Column(name = "instagram")
	private String instagram;

	@Column(name = "facebook")
	private String facebook;

	@Column(name = "twitter")
	private String twitter;

	@Column(name = "others")
	private String others;
}
