package com.james.salesman.model;

import javax.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


import static com.james.salesman.constant.ModuleConstant.*;

@EqualsAndHashCode( callSuper = true )
@Entity
@Table( name = TABLE_PREFIX + COUNTRY_STATE)
@NoArgsConstructor
@ToString
public class CountryState extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = COUNTRY_STATE)
	@SequenceGenerator(name = COUNTRY_STATE, sequenceName = SEQUENCE_PREFIX+COUNTRY_STATE)
	private Long id;

	@Column(name = "title")
	private String title;

	@ManyToOne(cascade = CascadeType.ALL, targetEntity = Country.class)
	@JoinColumn(name = "country_id", nullable = false)
	private Country country;


}
