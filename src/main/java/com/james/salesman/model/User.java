package com.james.salesman.model;

import java.time.LocalDate;
import java.util.Collection;
import javax.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;


import static com.james.salesman.constant.ModuleConstant.*;

@EqualsAndHashCode( callSuper = true )
@Entity
@Table( name = TABLE_PREFIX + USER )
@NoArgsConstructor
@ToString
@Data
public class User extends Base {
	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = USER )
	@SequenceGenerator( name = USER, sequenceName = SEQUENCE_PREFIX + USER )
	private Long id;

	@Column( name = "username", nullable = false, unique = true )
	private String username;

	@Column( name = "about" )
	private String about;

	@ManyToMany( cascade = CascadeType.MERGE, fetch = FetchType.EAGER )
	@JoinTable( name = TABLE_PREFIX + USER_ROLE, joinColumns = @JoinColumn( name = "user_id" ), inverseJoinColumns = @JoinColumn( name = "role_id" ) )
	private Collection< Role > roles;

	@Column( name = "password", nullable = false )
	private String password;

	@Column( name = "salt", nullable = false )
	private String salt;

	@OneToOne
	@JoinColumn( name = "staff_id" )
	private Staff staff;

	@OneToOne( cascade = CascadeType.ALL )
	@JoinColumn( name = "department_id" )
	private Department department;

	@Column( name = "is_enabled" )
	@ColumnDefault( "true" )
	private Boolean isEnabled = true;

	@Column( name = "is_account_locked" )
	@ColumnDefault( "true" )
	private Boolean isAccountLocked = false;

	@Column( name = "account_expire_on" )
	private LocalDate expireOn;

	@Column( name = "first_name", nullable = false )
	private String firstName;

	@Column( name = "last_name", nullable = false )
	private String lastName;


	public User(String firstName, String lastName, String username, String password, Collection< Role > roles) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
		this.roles = roles;
	}

	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}
}
