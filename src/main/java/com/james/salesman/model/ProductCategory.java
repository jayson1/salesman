package com.james.salesman.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+PRODUCT_CATEGORY)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class ProductCategory extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PRODUCT_CATEGORY)
	@SequenceGenerator(name = PRODUCT_CATEGORY, sequenceName = SEQUENCE_PREFIX+PRODUCT_CATEGORY)
	private Long id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "description")
	private String description;

	@Column(name = "formation")
	private String formation;

	@Column(name = "status")
	private Boolean status;

}
