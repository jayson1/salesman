package com.james.salesman.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+MEASUREMENT_UNIT)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MeasurementUnit extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = MEASUREMENT_UNIT)
	@SequenceGenerator(name = MEASUREMENT_UNIT, sequenceName = SEQUENCE_PREFIX+MEASUREMENT_UNIT)
	private Long id;

	@Column(name = "title", nullable = false)
	private String 	title;

	@Column(name = "hierarchy", nullable = false)
	@ColumnDefault("0")
	private String hierarchy;

	@Column(name = "status", nullable = false)
	@ColumnDefault("true")
	private Boolean status;

}
