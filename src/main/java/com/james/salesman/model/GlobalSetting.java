package com.james.salesman.model;

import javax.persistence.*;
import lombok.NoArgsConstructor;
import lombok.ToString;


import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+GLOBAL_SETTING)
@NoArgsConstructor
@ToString
public class GlobalSetting {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GLOBAL_SETTING)
	@SequenceGenerator(name = GLOBAL_SETTING, sequenceName = SEQUENCE_PREFIX+GLOBAL_SETTING)
	private Long id;

	@Column(nullable = false, unique = true, name = "title")
	private String title;

	@Column(name = "value")
	private String value;

	@Column(name = "section")
	private String section;

	public GlobalSetting(String title, String value, String section) {
		this.title = title;
		this.value = value;
		this.section = section;
	}
}
