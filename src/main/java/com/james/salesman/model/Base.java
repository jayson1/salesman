package com.james.salesman.model;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

@MappedSuperclass
@EntityListeners( AuditingEntityListener.class)
public class Base {
	@Temporal( TIMESTAMP)
	@Column(name = "deleted_at")
	protected Date deletedAt;

	@Temporal( TIMESTAMP)
	@LastModifiedDate
	@Column(name = "updated_at")
	protected Date updatedAt;

	@Temporal( TIMESTAMP)
	@CreatedDate
	@Column( updatable = false, name = "created_at")
	protected Date createdAt;

	@CreatedBy
	@Column(name = "created_by")
	protected String createdBy;

	@Column(name = "is_active")
	private Boolean isActive = true;
}
