package com.james.salesman.model;

import com.james.salesman.constant.MarginTypeEnum;
import com.james.salesman.constant.PaymentMethodEnum;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDate;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+SALE)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class Sale extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SALE)
	@SequenceGenerator(name = SALE, sequenceName = SEQUENCE_PREFIX+SALE)
	private Long id;

	@Column(name = "transaction_date")
	private LocalDate transactionDate;

	@OneToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@OneToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@Column(name = "payment_method", nullable = false)
	private PaymentMethodEnum paymentMethodEnum;

	@Column(name = "sub_total", nullable = false)
	@ColumnDefault( "0.00")
	private Double subTotal;

	@Column(name = "grand_total", nullable = false)
	@ColumnDefault("0.00")
	private Double grandTotal;

	@Column(name = "quantity_total", nullable = false)
	@ColumnDefault("0")
	private int quantityTotal;

	@Column(name = "note")
	private String note;

	@Column(name = "discount_total")
	@ColumnDefault("0.00")
	private Double discountTotal;

	@Column(name = "status")
	private Boolean status;

	@OneToOne
	@JoinColumn(name = "order_id")
	private Order order;

	@Column(name = "total_cost_price")
	@ColumnDefault("0.00")
	private Double totalCostPrice;

	@Column(name = "total_selling_price")
	@ColumnDefault("0.00")
	private Double totalSellingPrice;

	@Column(name = "margin_value")
	@ColumnDefault("0.00")
	private Double marginValue;

	@Column(name = "margin_type")
	private MarginTypeEnum marginType;

	@Column(name = "is_from_order")
	private Boolean isFromOrder;
}
