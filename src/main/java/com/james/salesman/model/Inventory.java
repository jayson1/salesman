package com.james.salesman.model;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDate;

import static com.james.salesman.constant.ModuleConstant.*;


@Entity
@Table( name = TABLE_PREFIX + INVENTORY )
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class Inventory extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = INVENTORY )
	@SequenceGenerator( name = INVENTORY, sequenceName = SEQUENCE_PREFIX+ INVENTORY )
	private Long id;

	@OneToOne
	@JoinColumn(name = "product_id", nullable = false)
	private Product product;

	@Column(name = "total_quantity")
	@ColumnDefault("0")
	private Integer totalQuantity;

	@OneToOne
	@JoinColumn(name = "measurement_unit_id", nullable = false)
	private MeasurementUnit measurementUnit;

	@Column(name = "total_cost")
	@ColumnDefault("0.00")
	private Double totalCost;

	@Column(name = "total_value")
	@ColumnDefault("0.00")
	private Double totalValue;

	@Column(name = "total_cost_inc_tax")
	@ColumnDefault("0.00")
	private Double totalCostTaxInc;

	@Column(name = "total_value_inc_tax")
	@ColumnDefault("0.00")
	private Double totalValueTaxInc;

	@Column(name = "count")
	@ColumnDefault("0")
	private int count;

	@OneToOne
	@JoinColumn(name = "supplier_id")
	private Supplier supplier;

	@Column(name = "entry_date")
	private LocalDate entryDate;


}
