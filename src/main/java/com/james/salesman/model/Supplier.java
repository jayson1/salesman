package com.james.salesman.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+SUPPLIER)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class Supplier extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SUPPLIER)
	@SequenceGenerator(name = SUPPLIER, sequenceName = SEQUENCE_PREFIX+SUPPLIER)
	private Long id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "phone")
	private String phone;

	@Column(name = "email")
	private String email;

	@Column(name = "other_name")
	private String otherName;

	@Column(name = "company_name")
	private String companyName;

	@Column(name = "address")
	private String address;

	@Column(name = "city")
	private String city;

	@OneToOne
	@JoinColumn(name = "country_id")
	private Country country;

	@OneToOne
	@JoinColumn(name = "state_id")
	private CountryState state;

	@Column(name = "registration_number")
	private String registrationNumber;

}
