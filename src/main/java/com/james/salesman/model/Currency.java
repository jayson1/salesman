package com.james.salesman.model;

import lombok.*;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table(name = TABLE_PREFIX + CURRENCY)
@EqualsAndHashCode(callSuper = true)
@Data @ToString  @NoArgsConstructor
public class Currency extends Base {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = CURRENCY)
	@SequenceGenerator(name = CURRENCY, sequenceName = SEQUENCE_PREFIX+CURRENCY)
	private Long id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "code")
	private String code;

}
