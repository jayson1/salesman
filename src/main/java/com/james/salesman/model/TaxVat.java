package com.james.salesman.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+TAX_VAT)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class TaxVat extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TAX_VAT)
	@SequenceGenerator(name = TAX_VAT, sequenceName = SEQUENCE_PREFIX+TAX_VAT)
	private Long id;

	@Column( name = "title")
	private String title;

	@Column(name = "value")
	private String value;

	@Column(name = "code")
	private String code;

	@Column(name = "is_fixed")
	private Boolean isFixed;

	@Column(name = "is_on_total")
	private Boolean isOnTotal;

}
