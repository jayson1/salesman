package com.james.salesman.model;

import java.util.Collection;
import javax.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


import static com.james.salesman.constant.ModuleConstant.*;

@Data
@Entity
@Table( name = TABLE_PREFIX + ROLE )
@EqualsAndHashCode( callSuper = true )
@ToString
@NoArgsConstructor
public class Role extends Base {
	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = ROLE )
	@SequenceGenerator( name = ROLE, sequenceName = SEQUENCE_PREFIX + ROLE )
	private Long id;

	@Column( name = "title" )
	private String title;

	@Column( name = "description" )
	private String description;

	@ManyToMany( cascade = CascadeType.MERGE, fetch = FetchType.EAGER )
	@JoinTable( name = TABLE_PREFIX + ROLE_PERM, joinColumns = @JoinColumn( name = "role_id" ), inverseJoinColumns = @JoinColumn( name = "permission_id" ) )
	private Collection< Permission > permissions;

	public Role(String title, String description, Collection< Permission > permissions) {
		this.title = title;
		this.description = description;
		this.permissions = permissions;
	}
}


