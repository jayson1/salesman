package com.james.salesman.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+ORGANIZATION)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class Organization extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ORGANIZATION)
	@SequenceGenerator(name = ORGANIZATION, sequenceName = SEQUENCE_PREFIX+ORGANIZATION)
	private Long id;

	@Column(name = "address")
	private String address;

	@Column(name = "phone")
	private String phone;

	@Column(name = "website_url")
	private String websiteUrl;

	@OneToOne
	@JoinColumn(name = "country_id")
	private Country country;

	@OneToOne
	@JoinColumn(name = "state_id")
	private CountryState state;

	@Column(name = "city")
	private String city;

	@Column(name = "profile")
	private String profile;

	@OneToOne
	@JoinColumn(name = "social_media_id")
	private SocialMedia socialMedia;

	@Lob
	@Type( type="org.hibernate.type.BinaryType")
	@Column(name = "logo")
	private Byte logo;
}
