package com.james.salesman.model;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+EXPENSES_CATEGORY)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class ExpensesCategory extends Base{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = EXPENSES_CATEGORY)
	@SequenceGenerator(name = EXPENSES_CATEGORY, sequenceName = SEQUENCE_PREFIX+EXPENSES_CATEGORY)
	private Long id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "description")
	private String description;
}
