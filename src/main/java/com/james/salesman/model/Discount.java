package com.james.salesman.model;

import com.james.salesman.constant.DiscountTypeEnum;
import lombok.*;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+DISCOUNT)
@EqualsAndHashCode( callSuper = true)
@ToString @Data  @NoArgsConstructor
public class Discount extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = DISCOUNT)
	@SequenceGenerator(name = DISCOUNT, sequenceName = SEQUENCE_PREFIX+DISCOUNT)
	private Long id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "discount_type")
	private DiscountTypeEnum discountType;

	@Column(name = "discount_value")
	private Double discountValue;


}
