package com.james.salesman.model;

import com.james.salesman.constant.GenderEnum;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+STAFF)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class Staff extends Base{

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = STAFF)
	@SequenceGenerator(name = STAFF, sequenceName = SEQUENCE_PREFIX+STAFF)
	private Long id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "other_name")
	private String otherName;

	@Column(name = "designation")
	private String designation;

	@Column(name = "gender")
	private GenderEnum gender;

	@Column(name = "mobile1")
	private String mobile1;

	@Column(name = "mobile2")
	private String mobile2;

	@Column(name = "email")
	private String email;

	@OneToOne
	@JoinColumn(name = "department_id")
	private Department department;

	@Column(name = "city")
	private String city;

	@Column(name = "address")
	private String address;

	@Column(name = "about")
	private String about;

	@Column(name = "date_of_birth")
	private LocalDate dateOfBirth;

	@Column(name = "date_hired")
	private LocalDate dateHired;

	@Column(name = "date_left")
	private LocalDate dateLeft;

//	@OneToOne(mappedBy = "staff")
//	private User user;

}
