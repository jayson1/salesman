package com.james.salesman.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;


@EqualsAndHashCode( callSuper = true )
@Entity
@Table( name = TABLE_PREFIX+BARCODE )
@NoArgsConstructor
@ToString
public class Barcode extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = BARCODE)
	@SequenceGenerator(name = BARCODE, sequenceName = SEQUENCE_PREFIX+BARCODE )
	private Long id;

	@Column(name = "value", nullable = false)
	private String value;

	@Column(name = "data")
	@Lob
	@Type( type="org.hibernate.type.BinaryType")
	private byte[] data;

}
