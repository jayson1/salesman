package com.james.salesman.model;

import com.james.salesman.constant.MarginTypeEnum;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+SERVICE)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class Service extends Base{

	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = SERVICE)
	@SequenceGenerator(name = SERVICE, sequenceName = SEQUENCE_PREFIX+SERVICE)
	private Long id;

	@Column(name = "title")
	private String title;

	@Column(name = "selling_price")
	private Double sellingPrice;

	@Column(name = "cost_price")
	private Double costPrice;

	@Column(name = "margin")
	@ColumnDefault("0.00")
	private Double margin;

	@Column(name = "margin_type")
	private MarginTypeEnum marginType;


}
