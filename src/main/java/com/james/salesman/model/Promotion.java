package com.james.salesman.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+PROMOTION)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class Promotion extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PROMOTION)
	@SequenceGenerator(name = PROMOTION, sequenceName = SEQUENCE_PREFIX+PROMOTION)
	private Long id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "start_date", nullable = false)
	private LocalDate startDate;

	@Column(name = "end_date", nullable = false)
	private LocalDate endDate;

	@Column(name = "start_time", nullable = false)
	private LocalTime startTime;

	@Column(name = "end_time", nullable = false)
	private LocalTime endTime;

	@Column(name = "days_of_week")
	private int daysOfWeek;

	@Column(name = "is_enabled")
	private Boolean isEnabled;

}
