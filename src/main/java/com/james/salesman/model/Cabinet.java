package com.james.salesman.model;


import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

import static com.james.salesman.constant.ModuleConstant.*;

@EqualsAndHashCode( callSuper = true )
@Entity
@Table( name = TABLE_PREFIX+CABINET )
@NoArgsConstructor
@ToString
public class Cabinet extends  Base{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = CABINET)
	@SequenceGenerator(name = CABINET, sequenceName = SEQUENCE_PREFIX+CABINET)
	private Long id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "position")
	private String position;

	@Column(name = "code")
	private String code;

	@ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	@JoinColumn(name = "cabinet_id")
	private Cabinet parent;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
	private List<Cabinet> children;
}
