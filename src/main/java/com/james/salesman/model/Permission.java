package com.james.salesman.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Data
@Entity
@Table(name = TABLE_PREFIX+PERMISSION)
@ToString @NoArgsConstructor @AllArgsConstructor
public class Permission {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PERMISSION)
	@SequenceGenerator(name = PERMISSION, sequenceName = SEQUENCE_PREFIX+PERMISSION)
	private Long id;

	@Column(nullable = false, unique = true, name = "title")
	private String title;

	@Column(name = "section")
	private String section;


	public Permission(String title, String section) {
		this.title = title;
		this.section = section;
	}
}
