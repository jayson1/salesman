package com.james.salesman.model;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table(name = TABLE_PREFIX+DISCOUNT_STRATEGY)
@EqualsAndHashCode( callSuper = true)
@ToString
@Data
@NoArgsConstructor
public class DiscountStrategy extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = DISCOUNT_STRATEGY)
	@SequenceGenerator(name = DISCOUNT_STRATEGY, sequenceName = SEQUENCE_PREFIX+DISCOUNT_STRATEGY)
	private Long id;

	@Column(name = "title", nullable = false, unique = true)
	private String title;

	@Column(name = "value")
	private String value;

	@Column(name = "status")
	private Boolean status;

	@Column(name = "hierarchy")
	@ColumnDefault("0")
	private int hierarchy;
}
