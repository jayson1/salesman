package com.james.salesman.model;

import com.james.salesman.constant.OperationStatusEnum;
import com.james.salesman.constant.OrderDeliveryOptionEnum;
import com.james.salesman.constant.OrderSourceEnum;
import java.time.LocalDate;
import javax.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;


import static com.james.salesman.constant.ModuleConstant.*;

@Entity
@Table( name = TABLE_PREFIX+ORDER)
@EqualsAndHashCode( callSuper = true)
@NoArgsConstructor
@ToString
public class Order extends Base{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ORDER)
	@SequenceGenerator(name = ORDER, sequenceName = SEQUENCE_PREFIX+ORDER)
	private Long id;

	@Column(name = "order_date")
	private LocalDate orderDate;

	@OneToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@Column(name = "total_amount")
	@ColumnDefault("0.00")
	private Double totalAmount;

	@Column(name = "order_source")
	private OrderSourceEnum source;

	@Column(name = "total_quantity")
	private Integer totalQuantity;

	@Column(name = "order_status")
	private OperationStatusEnum status;

	@Column(name = "date_sent")
	private LocalDate dateSent;

	@Column(name = "option")
	private OrderDeliveryOptionEnum option;

//	@OneToMany(mappedBy = "order")
//	private List<OrderItem> orderItems;


}
