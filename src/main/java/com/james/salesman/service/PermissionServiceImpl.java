package com.james.salesman.service;

import com.james.salesman.iservice.IPermissionService;
import com.james.salesman.model.Permission;
import com.james.salesman.repository.PermissionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static com.james.salesman.constant.MessagesConstant.*;

@Service @Slf4j
public class PermissionServiceImpl implements IPermissionService {


	private PermissionRepository permissionRepository;

	@Autowired
	public PermissionServiceImpl(PermissionRepository permissionRepository) {
		this.permissionRepository = permissionRepository;
	}

	@Override
	public Collection< Permission > seedApplicationPermission(Collection< Permission > permissions){
		log.info(SEED_STARTED);
		List< Permission > savedPermissions = new ArrayList<>();
		try{
			savedPermissions = this.permissionRepository.saveAll(permissions);
			log.info(SEEDED_SUCCESSFULLY);
		}catch( Exception e ){
			log.error(SEED_FAILED);
			e.printStackTrace();
		}
		return savedPermissions;
	}

	@Override
	public boolean hasAnyPermission(){
		return this.permissionRepository.count() > 0;
	}

	@Override
	public List<Permission> findAllPermission(){
		return this.permissionRepository.findAll();
	}

	@Override
	public HashSet<Permission> mapToPermissionList(List< String > permissions, String section){
		HashSet<Permission> permissionList = new HashSet<>();
		for( String title : permissions ){
			permissionList.add(new Permission(title, section));
		}
		return permissionList;
	}
}
