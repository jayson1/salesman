package com.james.salesman.service;

import com.james.salesman.constant.ErrorMessage;
import com.james.salesman.dto.Response;
import com.james.salesman.dto.UserInfo;
import com.james.salesman.iservice.IUserService;
import com.james.salesman.model.Permission;
import com.james.salesman.model.Role;
import com.james.salesman.model.User;
import com.james.salesman.repository.UserRepository;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import static com.james.salesman.constant.MessagesConstant.NOT_AVAILABLE;

@Service
@Slf4j
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User findByUsername(String username) {
		Optional< User > user = this.userRepository.findByUsername(username);
		if( user.isEmpty() ){
			//todo:: handle this case without throwing runtime exception -> refactor
			throw new RuntimeException("USER NOT FOUND");
		}
		return user.get();
	}

	@Override
	public Set< String > getUserRoleString(User user) {
		return user.getRoles().stream().map(Role::getTitle).collect(Collectors.toSet());
	}

	@Override
	public Set< String > getUserPermissionsString(User user) {
		return user.getRoles().stream()
				.flatMap(role -> role.getPermissions()
						.stream()).map(Permission::getTitle)
				.collect(Collectors.toSet());
	}

	@Override
	public void createUser(User dto){
		String salt = this.generateSalt().toHex();
		String hashPassword = this.hashPassword(dto.getPassword(), salt);
		dto.setSalt(salt);
		dto.setPassword(hashPassword);
		this.userRepository.save(dto);
	}

	@Override
	public boolean isUserNameExist(String username) {
		Optional< User > user = this.userRepository.findByUsername(username);
		return user.isPresent();
	}

	@Override
	public User authenticateUser(String username, String password) {
		User user = this.findByUsername(username);
		boolean isVerified = this.verifyPassword(user.getPassword(), password, user.getSalt());
		if( !isVerified ){
			throw new AuthenticationException(ErrorMessage.INVALID_LOGIN_CREDENTIALS);
		}
		return user;
	}

	@Override
	public Response login(String username, String password){
		Subject currentUser = SecurityUtils.getSubject();
		Response response = new Response();
		if (!currentUser.isAuthenticated()) {
			UsernamePasswordToken token = new UsernamePasswordToken(username, password);
			token.setRememberMe(false);
			try {
				currentUser.login(token);
				Session session = currentUser.getSession();
				session.setAttribute( "username", token.getUsername() );
				new SubjectThreadState(currentUser).bind();
				response.setStatus(true);
			}catch( AuthenticationException e ){
				response.setStatus(false);
				response.setMessages(Collections.singletonList(e.getMessage()));
			}
		}
		return response;
	}

	@Override
	public boolean logout(){
		try{
			Subject subject = SecurityUtils.getSubject();
			if( subject != null && subject.isAuthenticated() ){
				subject.logout();
				return true;
			}
			return false;
		}catch( Exception e){
			log.error("Failed to logout", e);
			return false;
		}
	}

	@Override
	public Optional<User> getActiveLoggedInUser(){
		if( !isAuthenticated() ){
			return Optional.empty();
		}
		Subject currentUser = SecurityUtils.getSubject();
		User principal = (User) currentUser.getPrincipal();
		return Optional.of(principal);
	}

	@Override
	public UserInfo getActiveUserInfo(){
		Optional< User > user = this.getActiveLoggedInUser();
		if( user.isEmpty() ){
			return new UserInfo(NOT_AVAILABLE, NOT_AVAILABLE);
		}
		return new UserInfo(user.get().getFullName(), user.get().getUsername());
	}

	private boolean isAuthenticated(){
		Subject currentUser = SecurityUtils.getSubject();
		return currentUser.isAuthenticated();
	}

	private boolean verifyPassword(String hashPassword, String plainPassword, String salt){
		String hashedPassword = this.hashPassword(plainPassword, salt);
		return hashedPassword.equals(hashPassword);
	}

	private String hashPassword(String plainPassword, String salt){
		return new Sha256Hash(plainPassword, salt, 1024).toBase64();
	}

	private ByteSource generateSalt(){
		RandomNumberGenerator rng = new SecureRandomNumberGenerator();
		return rng.nextBytes();
	}



}
