package com.james.salesman.service;

import com.james.salesman.iservice.IRoleService;
import com.james.salesman.model.Role;
import com.james.salesman.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service @Slf4j
public class RoleServiceImpl implements IRoleService {

	private RoleRepository roleRepository;

	@Autowired
	public RoleServiceImpl(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	@Override
	public Role createRole(Role role){
		Role saved = null;
		try {
			saved = this.roleRepository.save(role);
		}
		catch( Exception e ) {
			log.error(e.getMessage());
		}
		return saved;
	}

	@Override
	public void createManyRoles(List< Role > role){

	}

	@Override
	public boolean isRoleExist(String roleName) {
		Optional< Role > role = this.roleRepository.findByTitle(roleName);
		return role.isPresent();
	}

	@Override
	public Role findByTitle(String name){
		Optional< Role > role = this.roleRepository.findByTitle(name);
		if( role.isEmpty() ){
			throw new RuntimeException("Role not found");
		}
		return role.get();
	}
}
