package com.james.salesman.service;

import com.james.salesman.iservice.IGlobalSettingService;
import com.james.salesman.model.GlobalSetting;
import com.james.salesman.repository.GlobalSettingRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GlobalSettingServiceImpl implements IGlobalSettingService {
	@Autowired
	GlobalSettingRepository repository;

	@Override
	public boolean hasAnySetting(){
		long count = this.repository.count();
		return count > 0;
	}

	@Override
	public void seedSettings(List< GlobalSetting > globalSettingList) {
		if( globalSettingList.size() > 0 ){
			this.repository.saveAll(globalSettingList);
		}
	}
}
