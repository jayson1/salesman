package com.james.salesman.utils;

import com.james.salesman.configuration.SpringFxmlLoader;
import com.james.salesman.constant.ModuleEnum;
import com.james.salesman.dto.EventObj;
import com.james.salesman.dto.SceneInfoDto;
import com.james.salesman.iservice.IScene;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;


import static com.james.salesman.constant.DialogConstants.WHITE_FILL_STYLE;

@Slf4j
public class StageManager {

	private final Stage primaryStage;
	private final SpringFxmlLoader fxmlLoader;
	private Double tempHeight;
	private Double tempWidth;

	private SimpleObjectProperty< Object > a;
	private SimpleObjectProperty< Object > b;
	private SimpleObjectProperty< EventObj > out;

	public StageManager(SpringFxmlLoader fxmlLoader, Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.fxmlLoader = fxmlLoader;

		a = new SimpleObjectProperty<>();
		b = new SimpleObjectProperty<>();
		out = new SimpleObjectProperty<>();
		onInitEvent();
	}

	// starts
	public void setA(Object a) {
		this.a.set(a);
	}

	public void setB(Object b) {
		this.b.set(b);
	}

	public SimpleObjectProperty< EventObj > outProperty() {
		return out;
	}



	public void appendAndSwitchScene(final IScene parent, IScene child, SceneInfoDto infoDto) {
		StackPane root = ( StackPane ) this.loadRootNode(parent.getFXML());
		BorderPane borderPane = ( BorderPane ) root.getChildren().get(0);

		this.setBreadCrumbs(borderPane.getTop(), infoDto.getBreadCrumbs());

		this.setActiveNavMenu(borderPane.getLeft(), infoDto.getActiveModule());

		Parent childRoot = this.loadRootNode(child.getFXML());
		this.appendChild(childRoot, borderPane);
		this.setPrefSize(root, infoDto);
		this.show(root, child.getTitle(), infoDto);
	}

	public void switchScene(final IScene scene, SceneInfoDto sceneInfoDto) {
		Parent root = this.loadRootNode(scene.getFXML());
		this.setPrefSize(root, sceneInfoDto);
		this.show(root, scene.getTitle(), sceneInfoDto);
	}

	public Parent loadRootNodeAndUpdateSceneTitle(IScene scene){
		this.primaryStage.setTitle(scene.getTitle());
		return this.loadRootNode(scene.getFXML());
	}

	private Parent loadRootNode(String fxmlPath) {
		Parent parent = null;
		try {
			parent = this.fxmlLoader.load(fxmlPath);
		}
		catch( IOException e ) {
			this.logAndExit("Unable to load view " + fxmlPath, e);
		}
		return parent;
	}


	private void onInitEvent() {
		a.addListener(new ChangeListener< Object >() {
			@Override
			public void changed(ObservableValue< ? extends Object > observable, Object oldValue, Object newValue) {
				out.set(new EventObj("a changed", newValue));
			}
		});

		b.addListener(new ChangeListener< Object >() {
			@Override
			public void changed(ObservableValue< ? extends Object > observable, Object oldValue, Object newValue) {
				out.set(new EventObj("b changed", newValue));
			}
		});

		primaryStage.heightProperty().addListener((observableValue, oldValue, newValue) -> tempHeight = ( Double ) newValue);
		primaryStage.widthProperty().addListener((observableValue, oldValue, newValue) -> tempWidth = (Double ) newValue);
	}


	private void setBreadCrumbs(Node node, List< String > crumbs) {
		// bread crumb visibility is set to false in css.
		try{
			Label label = ( Label ) node.lookup("#breadCrumbs-link1");
			label.setText(crumbs.get(0));
			label.setStyle("visibility: 'true'"); // set visibility to true
			label.getStyleClass().add("bread-crumbs-link-black");
		}catch( Exception e){
			log.error(e.getMessage());
		}

	}

	private void setStyle(Node node, String style) {
		node.setStyle(style);
	}

	private void setActiveButton(HBox hBox) {
		JFXButton button = ( JFXButton ) hBox.getChildren().get(1);
		this.setStyle(button.getGraphic(), WHITE_FILL_STYLE);
	}

	private void setActiveNavMenu(Node node, ModuleEnum nav) {
		HBox dashboardNavWrapper = ( HBox ) node.lookup(nav.idValue());
		this.setStyle(dashboardNavWrapper.getChildren().get(0), WHITE_FILL_STYLE);
		this.setActiveButton(dashboardNavWrapper);
	}

	private void setPrefSize(Parent root, SceneInfoDto sceneInfoDto) {
		this.getScreenBounds();
		if( sceneInfoDto.getMaximized() && ( root instanceof StackPane ) ) {
			StackPane pane = ( StackPane ) root;
			pane.setPrefSize(tempWidth, tempHeight);
		}
		else if( sceneInfoDto.getMaximized() && root instanceof BorderPane ) {
			BorderPane pane = ( BorderPane ) root;
			pane.setPrefSize(tempWidth, tempHeight);
		}
	}

	private void getScreenBounds() {
		Screen screen = Screen.getPrimary();
		this.tempWidth = screen.getVisualBounds().getWidth();
		this.tempHeight = screen.getVisualBounds().getHeight();
	}

	private void show(final Parent parent, String title, SceneInfoDto sceneInfoDto) {
		Scene scene = this.prepareScene(parent);
		this.primaryStage.setTitle(title);
		this.primaryStage.setScene(scene);
		//this.primaryStage.sizeToScene();
		this.primaryStage.centerOnScreen();
		//this.primaryStage.setMaximized(sceneInfoDto.getMaximized());
		this.primaryStage.setMinWidth(sceneInfoDto.getMinWidth());
		this.primaryStage.setMinHeight(sceneInfoDto.getMinHeight());
		try {
			this.primaryStage.show();
		}
		catch( Exception e ) {
			this.logAndExit("Unable to show scene " + title, e);
		}
	}

	private void appendChild(Parent childRoot, BorderPane borderPane) {
		borderPane.getChildren().remove(borderPane.getCenter());
		borderPane.setCenter(childRoot);
	}

	private Scene prepareScene(Parent parent) {
		Scene scene = this.primaryStage.getScene();
		if( scene == null ) {
			scene = new Scene(parent);
		}
		scene.setRoot(parent);
		System.out.println("adding style");
		scene.getStylesheets().add(getClass().getResource("/css/theme-light.css").toExternalForm());
		return scene;
	}

	private void logAndExit(String errorMessage, Exception exception) {
		System.out.println(errorMessage);
		System.out.println(exception.getMessage());
		Platform.exit();
	}

}
