package com.james.salesman.utils;

import com.james.salesman.constant.GlobalSettingDefaultEnum;
import com.james.salesman.constant.PermissionsEnum;
import com.james.salesman.model.GlobalSetting;
import com.james.salesman.model.Permission;
import com.james.salesman.model.Role;
import com.james.salesman.model.User;
import com.james.salesman.service.GlobalSettingServiceImpl;
import com.james.salesman.service.PermissionServiceImpl;
import com.james.salesman.service.RoleServiceImpl;
import com.james.salesman.service.UserServiceImpl;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


import static com.james.salesman.constant.ModuleConstant.*;


@Component
public class SeedDB implements CommandLineRunner {

	private PermissionServiceImpl permissionService;
	private RoleServiceImpl roleService;
	private UserServiceImpl userService;
	private GlobalSettingServiceImpl settingService;

	private Collection< Permission > appPermissions = new ArrayList<>();
	private List< Role > roles = new ArrayList<>();


	@Value( "${salesman.seeder.start}" )
	private boolean canSeed;

	@Autowired
	public SeedDB(PermissionServiceImpl permissionService, RoleServiceImpl roleService, UserServiceImpl userService, GlobalSettingServiceImpl settingService) {
		this.permissionService = permissionService;
		this.roleService = roleService;
		this.userService = userService;
		this.settingService = settingService;
	}


	@Override
	public void run(String... args) throws Exception {
		if( this.canSeed ) {
			this.runPermissionSeeder();
			this.runRoleSeeder();
			this.runUserSeeder();
			this.runGlobalSettingSeeder();
		}
	}

	private void runGlobalSettingSeeder() {
		if( ! this.settingService.hasAnySetting() ) {
			List< GlobalSetting > globalSettingList = new ArrayList<>();
			EnumSet.allOf(GlobalSettingDefaultEnum.class).forEach(anEnum -> {
				globalSettingList.add(new GlobalSetting(anEnum.key(), anEnum.value(), anEnum.section()));
			});
			this.settingService.seedSettings(globalSettingList);
		}
	}

	private void runPermissionSeeder() {
		boolean hasAnyPermission = this.permissionService.hasAnyPermission();
		if( ! hasAnyPermission ) {

			this.appPermissions = new ArrayList<>();
			EnumSet.allOf(PermissionsEnum.class).forEach(permissionsEnum -> {
				HashSet< Permission > permissions = this.permissionService.mapToPermissionList(permissionsEnum.permissions(), permissionsEnum.section());
				this.appPermissions.addAll(permissions);
			});

			this.appPermissions = this.permissionService.seedApplicationPermission(this.appPermissions);
		}
	}

	private void runRoleSeeder() {
		if( ! this.roleService.isRoleExist(DEFAULT_ROLE) ) {
			Role appRole = new Role(DEFAULT_ROLE, DEFAULT_ROLE_DESC, this.appPermissions);
			Role role = this.roleService.createRole(appRole);
			this.roles.add(role);
		}

	}

	private void runUserSeeder() {
		if( ! this.userService.isUserNameExist(DEFAULT_USERNAME) ) {
			this.userService.createUser(new User(DEFAULT_USER_FIRST_NAME, DEFAULT_USER_LAST_NAME, DEFAULT_USERNAME, DEFAULT_PW, this.roles));
		}
	}

}
