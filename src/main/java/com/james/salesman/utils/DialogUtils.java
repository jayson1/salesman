package com.james.salesman.utils;

import com.james.salesman.dto.SceneInfoDto;
import com.james.salesman.dto.UserInfo;
import com.james.salesman.routes.MainApplicationRoute;
import com.james.salesman.service.UserServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXSpinner;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.shape.SVGPath;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;


import static com.james.salesman.constant.DialogConstants.*;
import static com.james.salesman.constant.SvgPathConstant.*;

/* handle creating dialog on screen, pass dialog content as Node => createDialog() */
@Service @Slf4j
public class DialogUtils {

	private JFXDialog dialog;
	private StackPane stackPane;

	@Autowired @Lazy
	private StageManager stageManager;
	@Autowired @Lazy
	private UserServiceImpl userService;

	/* creates a jfx dialog popup, attached to a stack pane as screen parent root*/
	public JFXDialog createDialog(StackPane parent, String title, Node body, boolean showCancel){
		this.stackPane =  parent;
		JFXDialogLayout dialogLayout = this.createDialogLayout(title, body, showCancel);
		this.dialog = new JFXDialog( parent, dialogLayout, JFXDialog.DialogTransition.CENTER);
		this.dialog.setOverlayClose(false);
		return dialog;
	}

	public JFXDialog createSpinner(StackPane container){
		JFXSpinner spinner = new JFXSpinner();
		// label
		Label label = this.createLabel(SPINNER_TEXT, "-fx-font-family: 'Roboto'; -fx-font-style: 'bold'");
		label.setAlignment(Pos.CENTER);

		VBox vBox = new VBox(10);
		vBox.setPadding(new Insets(10,10,10,10));
		vBox.getChildren().addAll(label, spinner);

		JFXDialog dialog = new JFXDialog(container, vBox, JFXDialog.DialogTransition.CENTER);
		dialog.setOverlayClose(false);
		return dialog;
	}

	/* creates a dialog layout with (logout, exit, shutdown) options when power button icon is clicked */
	public VBox createPowerDialogLayout(boolean showLogout){
		VBox vBox = new VBox();
		vBox.setSpacing(5);
		vBox.setPrefWidth(300);
		vBox.setStyle("-fx-background-color: '#FFFFFF;'");
		//create button
		JFXButton logout = this.createContentButton(LOGOUT, BTN_LOGOUT_TEXT);
		logout.setOnAction(e->this.onLogoutButtonAction());
		JFXButton exit = this.createContentButton(DESKTOP_CLOSE, BTN_EXIT_TEXT);
		exit.setOnAction(e->this.onExitButtonAction());
		JFXButton shutdown = this.createContentButton(TURN_OFF, BTN_SHUTDOWN_TEXT);
		shutdown.setOnAction(e->this.onShutdownButtonAction());
		//add button to vbox
		List<Node> nodes = new ArrayList<>();
		if( showLogout ) nodes.add(logout);
		nodes.add(exit);
		nodes.add(shutdown);
		nodes.forEach(e->vBox.getChildren().add(e));
		return vBox;
	}

	/* creates a warning message with icon displayed in dialog layout. (used user enter incorrect credentials) */
	public HBox warningLayout(String message){
		HBox hBox = new HBox();
		hBox.setAlignment(Pos.CENTER_LEFT);
		hBox.setSpacing(5);
		SVGPath svgPath = this.createSVGPath(INFORMATION, "-fx-fill: '#FF3333'");
		hBox.getChildren().addAll(svgPath, this.createLabel(message, "-fx-font-family: 'Roboto'"));
		return hBox;
	}

	// get logged in user details add to hbox container (display in top border pane across screen)
	public void addActiveUserToContainer(HBox container, String svgStyle, String topLabelStyle, String bottomLabelStyle){
		JFXButton notificationButton = this.createButton(this.createSVGPath(NOTIFICATION_PLUS, svgStyle), "", "");
		VBox userInfo = this.createActiveUserPanel(topLabelStyle, bottomLabelStyle);
		container.getChildren().clear();
		container.getChildren().addAll(notificationButton, userInfo);
	}

	// creates a vbox with 2 labels each for logged in (active ) user details
	private VBox createActiveUserPanel(String topLabelStyle, String bottomLabelStyle){
		VBox vBox = new VBox();
		vBox.prefHeight(58);
		vBox.setAlignment(Pos.CENTER_LEFT);
		VBox.setMargin(vBox, new Insets(0,10,0,0));
		HBox.setHgrow(vBox, Priority.NEVER);

		UserInfo activeUser = this.userService.getActiveUserInfo();
		Label fullName = this.createLabel(activeUser.getFullName(), topLabelStyle);
		fullName.getStyleClass().add(CSS_USER_FULL_NAME);
		Label username = this.createLabel(activeUser.getUsername(), bottomLabelStyle);

		vBox.getChildren().addAll(fullName, username);
		return vBox;
	}

	/* creates sgv icon wrapped in a button */
	private SVGPath createSVGPath(String svgIconPath, String style){
		SVGPath svgPath = new SVGPath();
		svgPath.setContent(svgIconPath);
		svgPath.setStyle(style);
		return svgPath;
	}

	/* creates a jfx dialog layout used to wrap dialog content before creating dialog popup */
	private JFXDialogLayout createDialogLayout(String title, Node node, boolean show){
		JFXDialogLayout dialogLayout = new JFXDialogLayout();
		if( show ) dialogLayout.setHeading(createDialogTitleCancel(title));
		else dialogLayout.setHeading(this.createLabel(title, "-fx-font-family: 'Roboto'"));
		dialogLayout.setBody(node);
		return dialogLayout;
	}

	/* create a label node */
	private Label createLabel(String text, String style){
		Label label = new Label(text);
		label.setStyle(style);
		HBox.setHgrow(label, Priority.ALWAYS);
		return label;
	}


	/* create a jfx button node  */
	private JFXButton createButton(SVGPath svgPath, String text, String style){
		JFXButton button = new JFXButton(text);
		button.setStyle(style);
		button.setGraphic(svgPath);
		HBox.setHgrow(button, Priority.ALWAYS);
		return button;
	}

	/* creates a region node for spacing */
	private Region createRegion(){
		Region region = new Region();
		HBox.setHgrow(region, Priority.ALWAYS);
		return region;
	}

	/* create jfx button used in power dialog options */
	private JFXButton createContentButton(String iconPath, String buttonText){
		JFXButton button = new JFXButton();
		button.setStyle("-fx-font-family: Roboto; font-size: '14px'; -fx-text-fill: '#000000';");
		button.setText(buttonText);
		button.setPrefHeight(45);
		button.setPrefWidth(290);
		button.setGraphicTextGap(10);
		button.setAlignment(Pos.TOP_LEFT);
		button.setPadding(new Insets(10,10,10,0));
		button.setGraphic(this.createSVGPath(iconPath, "-fx-fill: '#075e54'"));
		return button;
	}

	/* creates dialog heading with text and cancel button to close the dialog if user wants to close out*/
	private HBox createDialogTitleCancel(String text){
		HBox hBox = new HBox();
		hBox.setAlignment(Pos.CENTER_LEFT);
		JFXButton button = this.createButton(this.createSVGPath(CANCEL, ""), "", "");
		button.setOnAction(event -> this.dialog.close());
		HBox.setHgrow(button, Priority.ALWAYS);
		hBox.getChildren().addAll(this.createLabel(text, "-fx-font-family: 'Roboto'"), this.createRegion(), button);
		return hBox;
	}

	/* create confirm dialog before executing user request (used in logout, exit etc) */
	private JFXDialog createConfirmDialog(Node content, Node confirmButton){
		//todo:: reuse this.createDialog();
		JFXDialogLayout jfxDialogLayout = new JFXDialogLayout();
		jfxDialogLayout.setHeading(createDialogTitleCancel(WARNING_TEXT));
		jfxDialogLayout.setBody(content);
		jfxDialogLayout.setActions(confirmButton);
		JFXDialog dialog = new JFXDialog(this.stackPane, jfxDialogLayout, JFXDialog.DialogTransition.CENTER);
		dialog.setOverlayClose(false);
		return dialog;
	}

	/* action when user clicks logout button on power dialog */
	private void onLogoutButtonAction(){
		JFXButton button = this.createButton(this.createSVGPath(THUMBS_UP, ""),  BTN_LOGOUT_CONFIRM_TEXT, "-fx-background-color: '##FF3333'");
		button.setOnAction(event -> {
			this.userService.logout();
			this.stageManager.switchScene(MainApplicationRoute.LOGIN, new SceneInfoDto(true));
		});
		HBox content = this.warningLayout(CONFIRM_TEXT);
		JFXDialog confirmDialog = this.createConfirmDialog(content, button);
		this.dialog.close();
		this.dialog = confirmDialog;
		confirmDialog.show();
	}

	/* action when user clicks exit button on power dialog */
	private void onExitButtonAction(){
		JFXButton button = this.createButton(this.createSVGPath(THUMBS_UP, ""),  BTN_EXIT_CONFIRM_TEXT, "background-color: '##FF3333'");
		button.setOnAction(event -> {
			this.userService.logout();
			Platform.exit();
		});
		HBox content = this.warningLayout(CONFIRM_TEXT);
		JFXDialog confirmDialog = this.createConfirmDialog(content, button);
		this.dialog.close();
		this.dialog = confirmDialog;
		confirmDialog.show();
	}

	/* action when user clicks shutdown button on power dialog */
	private void onShutdownButtonAction(){
		System.out.println("Shutdown is clicked");
	}


}
