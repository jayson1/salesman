package com.james.salesman.utils;

import com.james.salesman.dto.FileModule;
import com.james.salesman.iservice.IFileHandler;
import org.springframework.stereotype.Service;

@Service
public class FileHandler implements IFileHandler {

	private FileModule fileModule;

	public void loadFile(String path){}

	public void importFile(){}

	public void exportFile(){}

	public  void validate(){}

	public void notifyStatus(){}
}
