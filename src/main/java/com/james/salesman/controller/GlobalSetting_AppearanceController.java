package com.james.salesman.controller;

import com.james.salesman.utils.DialogUtils;
import com.james.salesman.utils.StageManager;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

@Controller
@Slf4j
public class GlobalSetting_AppearanceController implements Initializable {
	private DialogUtils dialogUtils;
	private final StageManager stageManager;

	@Autowired
	@Lazy
	public GlobalSetting_AppearanceController(StageManager stageManager, DialogUtils dialogUtils) {
		this.stageManager = stageManager;
		this.dialogUtils = dialogUtils;
	}


	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
	}


}
