package com.james.salesman.controller;

import com.james.salesman.dto.SceneInfoDto;
import com.james.salesman.routes.DashboardRoute;
import com.james.salesman.routes.GlobalSettingRoute;
import com.james.salesman.routes.MainApplicationRoute;
import com.james.salesman.routes.ProductRoute;
import com.james.salesman.utils.DialogUtils;
import com.james.salesman.utils.StageManager;
import com.jfoenix.controls.JFXDialog;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;


import static com.james.salesman.constant.DialogConstants.*;
import static com.james.salesman.constant.ModuleEnum.*;

@Controller
@Slf4j
public class WelcomeController implements Initializable {

	private final StageManager stageManager;
	private DialogUtils dialogUtils;
	@FXML
	private StackPane stackpane;
	@FXML
	private HBox hBoxNotifs;
	private JFXDialog spinner;

	@Autowired
	@Lazy
	public WelcomeController(StageManager stageManager, DialogUtils dialogUtils) {
		this.stageManager = stageManager;
		this.dialogUtils = dialogUtils;
	}

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		//todo refactor active user profile, add only two labels to the hbox container
		this.dialogUtils.addActiveUserToContainer(this.hBoxNotifs, WHITE_FILL_STYLE, WHITE_ACTIVE_USER_TOP_LABEL_STYLE, WHITE_ACTIVE_USER_BOTTOM_LABEL_STYLE_);
		Platform.runLater(() -> this.stackpane.requestFocus());
	}

	@FXML
	private void handleDashboard(ActionEvent event) {
		this.stageManager.appendAndSwitchScene(MainApplicationRoute.ADMIN_MAIN, DashboardRoute.MAIN, new SceneInfoDto(true, List.of(DASHBOARD.moduleTitle()), DASHBOARD));
	}

	@FXML
	private void handleProductService() {
		this.stageManager.appendAndSwitchScene(MainApplicationRoute.ADMIN_MAIN, ProductRoute.MAIN, new SceneInfoDto(true, List.of(PRODUCT_AND_SERVICE.moduleTitle()), PRODUCT_AND_SERVICE));
	}

	@FXML
	private void handleStockInventory() {
		this.stageManager.appendAndSwitchScene(MainApplicationRoute.ADMIN_MAIN, ProductRoute.MAIN, new SceneInfoDto(true, List.of(STOCK_AND_INVENTORY.moduleTitle()), STOCK_AND_INVENTORY));
	}

	@FXML
	private void handleCustomerSupplier() {
		this.stageManager.appendAndSwitchScene(MainApplicationRoute.ADMIN_MAIN, ProductRoute.MAIN, new SceneInfoDto(true, List.of(CUSTOMERS_AND_SUPPLIERS.moduleTitle()), CUSTOMERS_AND_SUPPLIERS));

	}

	@FXML
	private void handleDiscountPromotion() {
		this.stageManager.appendAndSwitchScene(MainApplicationRoute.ADMIN_MAIN, ProductRoute.MAIN, new SceneInfoDto(true, List.of(DISCOUNT_AND_PROMOTION.moduleTitle()), DISCOUNT_AND_PROMOTION));
	}

	@FXML
	private void handleReturnExchange() {
		this.stageManager.appendAndSwitchScene(MainApplicationRoute.ADMIN_MAIN, ProductRoute.MAIN, new SceneInfoDto(true, List.of(RETURN_AND_EXCHANGE.moduleTitle()
		                                                                                                                         ), RETURN_AND_EXCHANGE));
	}

	@FXML
	private void handleCompany() {
		this.stageManager.appendAndSwitchScene(MainApplicationRoute.ADMIN_MAIN, ProductRoute.MAIN, new SceneInfoDto(true, List.of(COMPANY.moduleTitle()), COMPANY));
	}

	@FXML
	private void handleReport() {
		this.stageManager.appendAndSwitchScene(MainApplicationRoute.ADMIN_MAIN, ProductRoute.MAIN, new SceneInfoDto(true, List.of(REPORTS.moduleTitle()), REPORTS));
	}

	@FXML
	private void handleGlobalSetting() {
		this.stageManager.appendAndSwitchScene(MainApplicationRoute.ADMIN_MAIN, GlobalSettingRoute.GLOBAL_SETTING_MAIN, new SceneInfoDto(true, List.of(GLOBAL_SETTING.moduleTitle()), GLOBAL_SETTING));
	}

	@FXML
	private void handleSellProduct() {
		System.out.println("Loading Sell Product");
	}

	@FXML
	void onPowerButtonClick(ActionEvent event) {
		try {
			Node node = this.dialogUtils.createPowerDialogLayout(true);
			this.dialogUtils.createDialog(stackpane, POWER_DIALOG_TITLE, node, true).show();
		}
		catch( Exception e ) {
			log.error(e.getMessage(), e);
			Platform.exit();
		}
	}


}
