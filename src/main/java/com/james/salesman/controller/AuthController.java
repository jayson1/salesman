package com.james.salesman.controller;

import com.james.salesman.constant.DialogConstants;
import com.james.salesman.dto.Response;
import com.james.salesman.dto.SceneInfoDto;
import com.james.salesman.routes.MainApplicationRoute;
import com.james.salesman.service.UserServiceImpl;
import com.james.salesman.utils.DialogUtils;
import com.james.salesman.utils.StageManager;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;


import static com.james.salesman.constant.DialogConstants.POWER_DIALOG_TITLE;

@Controller @Slf4j
public class AuthController implements Initializable {

	private DialogUtils dialogUtils;
	private UserServiceImpl userService;
	private final StageManager stageManager;

	@FXML
	public JFXTextField txtEmail;
	@FXML
	public JFXPasswordField txtPassword;
	@FXML
	public JFXButton btnLogin;
	@FXML
	private StackPane stackPane;


	@Autowired
	@Lazy
	public AuthController(DialogUtils dialogUtils, UserServiceImpl userService, StageManager stageManager) {
		this.dialogUtils = dialogUtils;
		this.userService = userService;
		this.stageManager = stageManager;
	}

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		Platform.runLater(() -> {
			txtEmail.requestFocus();
			txtEmail.setText("superadmin");
			txtPassword.setText("password");
		});
	}

	@FXML
	public void onLoginButtonClicked(ActionEvent actionEvent) {
		String username = this.txtEmail.getText();
		String password = this.txtPassword.getText();
		if( username != null && password != null ){
			Response response = this.userService.login(username, password);
			if( response.isStatus() ) {
				this.stageManager.switchScene(MainApplicationRoute.WELCOME, new SceneInfoDto(true));
			}
			else {
				//todo::refactor the validation error message, display the actual error that occurred eg. account locked, incorrect login etc
				HBox message = this.dialogUtils.warningLayout(DialogConstants.LOGIN_ERROR_MESSAGE);
				JFXDialog dialog = this.dialogUtils.createDialog(stackPane, DialogConstants.LOGIN_ERROR_DIALOG_TITLE, message, true);
				this.stackPane.requestFocus();
				dialog.show();
			}
		}
	}


	@FXML
	public void onPowerButtonClick(ActionEvent event) {
		Node node = this.dialogUtils.createPowerDialogLayout(false);
		this.dialogUtils.createDialog(stackPane, POWER_DIALOG_TITLE, node, true).show();
	}
}
