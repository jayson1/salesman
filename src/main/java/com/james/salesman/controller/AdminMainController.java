package com.james.salesman.controller;

import com.james.salesman.dto.SceneInfoDto;
import com.james.salesman.routes.MainApplicationRoute;
import com.james.salesman.service.MainService;
import com.james.salesman.utils.DialogUtils;
import com.james.salesman.utils.StageManager;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;


import static com.james.salesman.constant.DialogConstants.*;

@Component @Slf4j
public class AdminMainController implements Initializable {
	@FXML
	public JFXButton btnDashboard;
	@FXML
	public JFXButton btnProduct;
	@FXML
	public JFXButton btnInventory;
	@FXML
	public JFXButton btnCustomers;
	@FXML
	public JFXButton btnSupplier;
	@FXML
	public JFXButton btnPromotion;
	@FXML
	public JFXButton btnReport;
	@FXML
	public JFXButton btnStaffRoles;
	@FXML
	public JFXButton btnGlobalSetting;
	@FXML
	public JFXButton btnTools;
	@FXML
	public JFXButton btnHelpCenter;
	@FXML
	public BorderPane mainBorderPane;
	@FXML
	public StackPane stackPane;
	@FXML
	public HBox userBox;


	private final MainService mainService;
	private final StageManager stageManager;
	private DialogUtils dialogUtils;

	@Autowired @Lazy
	public AdminMainController(MainService mainService, StageManager stageManager, DialogUtils dialogUtils) {
		this.mainService = mainService;
		this.stageManager = stageManager;
		this.dialogUtils = dialogUtils;
	}


	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		this.dialogUtils.addActiveUserToContainer(this.userBox, PRIMARY_FILL_STYLE, PRIMARY_ACTIVE_USER_TOP_LABEL_STYLE, PRIMARY_ACTIVE_USER_BOTTOM_LABEL_STYLE);
//		this.stageManager.outProperty().addListener(new ChangeListener< EventObj >(){
//			@Override
//			public void changed(ObservableValue<? extends EventObj> observable, EventObj oldValue, EventObj newValue){
//				System.out.println(newValue.getMessage() + ", new value is : " + newValue.getNewObj());
//			}
//		});
	}

	@FXML
	void onHomeClick(ActionEvent event) {
		this.stageManager.switchScene(MainApplicationRoute.WELCOME, new SceneInfoDto(true));
	}

	@FXML
	public void onBtnDashboardClicked(){
//		this.stageManager.loadChildView(DashboardRoute.MAIN, mainBorderPane);
	}
	@FXML
	public void onBtnProductClicked(){
//		this.stageManager.loadChildView(ProductRoute.MAIN, mainBorderPane);
	}
	@FXML
	public void onBtnInventoryClicked(){
		stageManager.setA(new Object());
		stageManager.setB(new Object());
	}
	@FXML
	public void onBtnCustomersClicked(){
	}
	@FXML
	public void onBtnSupplierClicked(){
	}
	@FXML
	public void onBtnPromotionClicked(){
	}
	@FXML
	public void onBtnReportClicked(){
	}
	@FXML
	public void onBtnStaffRolesClicked(){
	}
	@FXML
	public void onBtnGlobalSettingClicked(){
	}
	@FXML
	public void onBtnToolsClicked(){
	}
	@FXML
	public void onBtnHelpCenterClicked(){
	}
	@FXML
	public void onPowerButtonClick(){
		try{
			Node node = this.dialogUtils.createPowerDialogLayout(true);
			this.dialogUtils.createDialog(stackPane, POWER_DIALOG_TITLE, node, true).show();
		}catch( Exception e ){
			log.error(e.getMessage(), e);
			Platform.exit();
		}
	}


}
