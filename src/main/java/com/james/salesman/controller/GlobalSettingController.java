package com.james.salesman.controller;

import com.james.salesman.utils.DialogUtils;
import com.james.salesman.utils.StageManager;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.VBox;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;


import static com.james.salesman.routes.GlobalSettingRoute.GLOBAL_SETTING_APPEARANCE;

@Controller
@Slf4j
public class GlobalSettingController implements Initializable {

	private DialogUtils dialogUtils;
	private final StageManager stageManager;

	@FXML
	private JFXButton btnAppearance;

	@FXML
	private JFXButton btnDashboard;

	@FXML
	private JFXButton btnProductsServices;

	@FXML
	private JFXButton btnStockInventory;

	@FXML
	private JFXButton btnReturnExchange;

	@FXML
	private JFXButton btnEmail;

	@FXML
	private JFXButton btnPayment;

	@FXML
	private JFXButton btnTaxVat;

	@FXML
	private JFXButton btnExpenses;

	@FXML
	private JFXButton btnPrint;

	@FXML
	private VBox vbWrapper;

	@FXML
	private SplitPane splitWrapper;

	@Autowired
	@Lazy
	public GlobalSettingController(StageManager stageManager, DialogUtils dialogUtils) {
		this.stageManager = stageManager;
		this.dialogUtils = dialogUtils;
	}


	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		Platform.runLater(() -> splitWrapper.requestFocus());
	}

	private void addNode(Node n){
//		if( splitWrapper.getItems().size() > 1 ){
//			splitWrapper.getItems().remove(1);
//		}
//		splitWrapper.getItems().add(n);
	}

	@FXML
	void onBtnAppearanceAction(ActionEvent event) {
		Parent parent = this.stageManager.loadRootNodeAndUpdateSceneTitle(GLOBAL_SETTING_APPEARANCE);
		this.splitWrapper.getItems().remove(1);

//		if( this.splitWrapper.getItems().size() > 1 ){
//			this.splitWrapper.getItems().remove(1);
//		}
		this.splitWrapper.getItems().add(parent);
		this.splitWrapper.setDividerPosition(0,0.2);
	}

	@FXML
	void onBtnDashboardAction(ActionEvent event) {

	}

	@FXML
	void onBtnExpensesAction(ActionEvent event) {

	}

	@FXML
	void onBtnPaymentAction(ActionEvent event) {

	}

	@FXML
	void onBtnPrintAction(ActionEvent event) {

	}

	@FXML
	void onBtnProductServiceAction(ActionEvent event) {

	}

	@FXML
	void onBtnReturnExchangeAction(ActionEvent event) {

	}

	@FXML
	void onBtnStockInventoryAction(ActionEvent event) {

	}

	@FXML
	void onBtnTaxVatAction(ActionEvent event) {

	}

	@FXML
	void onEmailAction(ActionEvent event) {

	}



}
