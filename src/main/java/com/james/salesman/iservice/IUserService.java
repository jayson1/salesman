package com.james.salesman.iservice;

import com.james.salesman.dto.Response;
import com.james.salesman.dto.UserInfo;
import com.james.salesman.model.User;

import java.util.Optional;
import java.util.Set;

public interface IUserService {
	User findByUsername(String username);

	Set<String> getUserRoleString(User user);

	Set< String> getUserPermissionsString(User user);

	void createUser(User user);

	boolean isUserNameExist(String username);

	User authenticateUser(String username, String password);

	Response login(String username, String password);

	boolean logout();

	Optional<User> getActiveLoggedInUser();

	UserInfo getActiveUserInfo();
}
