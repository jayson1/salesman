package com.james.salesman.iservice;

public interface IScene {
	String getTitle();
	String getFXML();
}
