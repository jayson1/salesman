package com.james.salesman.iservice;

import com.james.salesman.model.GlobalSetting;
import java.util.List;

public interface IGlobalSettingService {
	boolean hasAnySetting();

	void seedSettings(List< GlobalSetting> globalSettingList);
}
