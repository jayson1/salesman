package com.james.salesman.iservice;

import com.james.salesman.model.Role;

import java.util.List;

public interface IRoleService {
	Role createRole(Role role);

	void createManyRoles(List< Role > role);

	boolean isRoleExist(String roleName);

	Role findByTitle(String name);
}
