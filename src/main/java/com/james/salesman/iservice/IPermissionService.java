package com.james.salesman.iservice;

import com.james.salesman.model.Permission;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public interface IPermissionService {
	Collection< Permission > seedApplicationPermission(Collection< Permission > permissions);

	boolean hasAnyPermission();

	List<Permission> findAllPermission();

	HashSet<Permission> mapToPermissionList(List< String > permissions, String section);
}
